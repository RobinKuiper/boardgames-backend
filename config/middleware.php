<?php

declare(strict_types = 1);

use App\Config;
use App\Enum\AppEnvironment;
use App\Enum\StatusCode;
use App\Middleware\CorsMiddleware;
use App\Middleware\TrailingSlashMiddleware;
use Clockwork\Clockwork;
use Clockwork\Support\Slim\ClockworkMiddleware;
use Monolog\Logger;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\App;
use Slim\Middleware\MethodOverrideMiddleware;

return static function (App $app) {
    $container     = $app->getContainer();
    $config        = $container?->get(Config::class);
    $isDevelopment = AppEnvironment::isDevelopment($config->get('environment'));
    $debug         = $config->get('debug');

    $app->add(MethodOverrideMiddleware::class);
    $app->add(TrailingSlashMiddleware::class);
    $app->add(CorsMiddleware::class);

    if ($isDevelopment && $debug) {
        $app->add(new ClockworkMiddleware($app, $container?->get(Clockwork::class)));
    }
    $app->addBodyParsingMiddleware();

    $errorMiddleware = $app->addErrorMiddleware(
        (bool)$config->get('display_error_details'),
        (bool)$config->get('log_errors'),
        (bool)$config->get('log_error_details'),
        $container?->get(Logger::class) // Doesn't do anything at the moment due to a bug
    );

    /**
     * Handle error and generate response based on given parameters.
     *
     * @param ServerRequestInterface $request the server request object
     * @param Throwable $exception the thrown exception
     * @param bool $displayErrorDetails flag to determine if error details should be displayed
     * @param bool $logErrors flag to determine if errors should be logged
     * @param bool $logErrorDetails flag to determine if error details should be logged
     *
     * @throws JsonException
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     *
     * @return ResponseInterface the generated response object
     */
    $customErrorHandler = function (
        ServerRequestInterface $request,
        Throwable $exception,
        bool $displayErrorDetails,
        bool $logErrors,
        bool $logErrorDetails,
        //        ?LoggerInterface $logger = null // Apparently this is broken in Slim
    ) use ($app) {
        $logger = $app->getContainer()?->get(Logger::class); // Workaround to get the Logger instance
        $config = $app->getContainer()?->get(Config::class);

        $logMessage = '';

        if ($logErrors) {
            $logMessage .= $exception->getMessage()
              . ' (file:' . $exception->getFile() . ':'
              . $exception->getLine() . ')';
        }

        if ($logErrors && $logErrorDetails) {
            $logMessage .= PHP_EOL;
        }

        if ($logErrorDetails) {
            $logMessage .= $exception->getTraceAsString();
        }

        $logger?->error($logMessage);

        $payload = [
            'ok'      => false,
            'message' => $exception->getMessage(),
        ];

        if (isset($exception->errors)) {
            $payload['errors'] = $exception->errors;
        }

        if ($displayErrorDetails) {
            $payload['status_code'] = $exception->getCode();
            $payload['stack_trace'] = $exception->getTrace();
        }

        $code =
          StatusCode::tryFrom($exception->getCode())->value ??
          StatusCode::InternalServerError->value;

        $response = $app->getResponseFactory()->createResponse($code);
        $response = $response
            ->withHeader('Access-Control-Allow-Origin', $config->get('cors.origin'))
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Credentials', 'true')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS')
            ->withHeader('Content-Type', 'application/json');
        $response->getBody()->write(json_encode($payload, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE));

        return $response;
    };

    $errorMiddleware->setDefaultErrorHandler($customErrorHandler);
};
