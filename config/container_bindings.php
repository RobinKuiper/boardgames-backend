<?php

declare(strict_types = 1);

use App\Config;
use App\Enum\AppEnvironment;
use App\Interfaces\EntityManagerServiceInterface;
use App\Interfaces\RequestValidatorFactoryInterface;
use App\Mailer;
use App\RouteEntityBindingStrategy;
use App\Services\EntityManagerService;
use App\Validators\RequestValidatorFactory;
use Clockwork\Clockwork;
use Clockwork\DataSource\DoctrineDataSource;
use Clockwork\Storage\FileStorage;
use Doctrine\Common\EventManager;
use Doctrine\DBAL\DriverManager;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Driver\AttributeDriver;
use Doctrine\Persistence\Mapping\Driver\MappingDriverChain;
use Gedmo\Blameable\BlameableListener;
use Gedmo\DoctrineExtensions;
use Gedmo\Loggable\LoggableListener;
use Gedmo\Mapping\Driver\AttributeReader;
use Gitlab\Client as GitlabClient;
use Http\Discovery\Psr17Factory;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ServerRequestFactoryInterface;
use Slim\App;
use Slim\Factory\AppFactory;
use Slim\Interfaces\RouteParserInterface;
use Symfony\Component\Cache\Adapter\ApcuAdapter;
use Symfony\Component\Cache\Adapter\ArrayAdapter;

use function DI\create;

return [
    App::class => function (ContainerInterface $container, Config $config) {
        AppFactory::setContainer($container);

        $app = AppFactory::create();

        $app->getRouteCollector()->setDefaultInvocationStrategy(
            new RouteEntityBindingStrategy(
                $container->get(EntityManagerServiceInterface::class)
            )
        );

        (require CONFIG_PATH . '/middleware.php')($app);
        (require CONFIG_PATH . '/routes.php')($app);

        $routeCollector = $app->getRouteCollector();

        if (AppEnvironment::isProduction($config->get('environment'))) {
            $routeCollector->setCacheFile(CACHE_PATH . '/router.cache');
        }

        return $app;
    },

    Config::class => create(Config::class)->constructor(require CONFIG_PATH . '/app.php'),

    EntityManagerInterface::class => function (Config $config, ContainerInterface $container) {
        $devMode = $config->get('doctrine.dev_mode');
        $cache   = $devMode ? new ArrayAdapter() : new ApcuAdapter();

        $annotationReader = null;

        // For PHP 8, we will provide the extensions an attribute reader, while PHP 7 will require the annotation reader
        // (which will only be created when `doctrine/annotations` is installed)
        $extensionReader = new AttributeReader();

        // Create the mapping driver chain that will be used to read metadata from our various sources.
        $mappingDriver = new MappingDriverChain();

        // Load the superclass metadata mapping for the extensions into the driver chain.
        // Internally, this will also register the Doctrine Extensions annotations.
        DoctrineExtensions::registerAbstractMappingIntoDriverChainORM(
            $mappingDriver,
            $annotationReader
        );

        // Register the application entities to our driver chain.
        // Our application uses Annotations or Attributes for mapping, but you can also use XML.
        $mappingDriver->addDriver(
            new AttributeDriver(
                $config->get('doctrine.entity_dir')
            ),
            'App\Entity'
        );

        // create event manager and hook preferred extension listeners
        $eventManager = new EventManager();

        $blameableListener = $container->get(BlameableListener::class);
        $blameableListener->setAnnotationReader($extensionReader);
        $blameableListener->setCacheItemPool($cache);
        $eventManager->addEventSubscriber($blameableListener);

        $loggableListener = $container->get(LoggableListener::class);
        $loggableListener->setAnnotationReader($extensionReader);
        $loggableListener->setCacheItemPool($cache);
        //        $loggableListener->setUsername('system');
        $eventManager->addEventSubscriber($loggableListener);

        // Now we will build our ORM configuration.
        $ormConfig = new Configuration();
        $ormConfig->setProxyDir(sys_get_temp_dir());
        $ormConfig->setProxyNamespace('DoctrineProxy');
        $ormConfig->setAutoGenerateProxyClasses($devMode);
        $ormConfig->setMetadataDriverImpl($mappingDriver);
        $ormConfig->setMetadataCache($cache);
        $ormConfig->setQueryCache($cache);
        $ormConfig->setResultCache($cache);

        //        if (class_exists('DoctrineExtensions\Query\Mysql\Year')) {
        //            $ormConfig->addCustomDatetimeFunction('YEAR', Year::class);
        //        }
        //
        //        if (class_exists('DoctrineExtensions\Query\Mysql\Month')) {
        //            $ormConfig->addCustomDatetimeFunction('MONTH', Month::class);
        //        }
        //
        //        if (class_exists('DoctrineExtensions\Query\Mysql\DateFormat')) {
        //            $ormConfig->addCustomStringFunction('DATE_FORMAT', DateFormat::class);
        //        }

        $connection = DriverManager::getConnection($config->get('doctrine.connection'), $ormConfig);

        return new EntityManager(
            $connection,
            $ormConfig,
            $eventManager
        );
    },

    BlameableListener::class => fn () => new BlameableListener(),
    LoggableListener::class => fn () => new LoggableListener(),

    //    AuthInterface::class => fn (ContainerInterface $container) => $container->get(
    //        Auth::class
    //    ),

    //    UserProviderServiceInterface::class => fn (ContainerInterface $container) => $container->get(
    //        UserProviderService::class
    //    ),

    EntityManagerServiceInterface::class => fn (EntityManagerInterface $entityManager) => new EntityManagerService(
        $entityManager
    ),

    Logger::class => function () {
        $logger = new Logger('daily');
        $rotating_handler = new RotatingFileHandler(__DIR__ . '/../log/debug.log', DEFAULT_MAX_LOG_FILES);
        $output = "%level_name% | %datetime% > %message% | %context% %extra%\n";
        $dateFormat = 'j-n-Y, g:i a';
        $formatter = new LineFormatter(
            $output, // Format of message in log
            $dateFormat, // Datetime format
            true, // allowInlineLineBreaks option, default false
            true  // discard empty Square brackets in the end, default false
        );
        $rotating_handler->setFormatter($formatter);
        $logger->pushHandler($rotating_handler);

        return $logger;
    },

    RequestValidatorFactoryInterface::class => fn (ContainerInterface $container) => $container->get(
        RequestValidatorFactory::class
    ),

    ResponseFactoryInterface::class => fn (App $app) => $app->getResponseFactory(),

    RouteParserInterface::class => fn (App $app) => $app->getRouteCollector()->getRouteParser(),

    Clockwork::class => function (EntityManagerInterface $entityManager) {
        $clockwork = new Clockwork();

        $clockwork->storage(new FileStorage(STORAGE_PATH . '/clockwork'));
        assert($entityManager instanceof EntityManager);
        $clockwork->addDataSource(new DoctrineDataSource($entityManager));

        return $clockwork;
    },

    ServerRequestFactoryInterface::class => function (ContainerInterface $container) {
        return $container->get(Psr17Factory::class);
    },

    GitlabClient::class => function (Config $config) {
        $client = new GitlabClient();
        $client->authenticate($config->get('gitlab.token'), GitlabClient::AUTH_HTTP_TOKEN);

        return $client;
    },

    Mailer::class => function (Config $config) {
        $devMode = $config->get('mailer.dev_mode');
        $useAuth = $config->get('mailer.use_auth');

        $mailer = new Mailer($devMode);
        $mailer->SMTPDebug = $devMode ? SMTP::DEBUG_SERVER : SMTP::DEBUG_OFF;
        $mailer->Host = $config->get('mailer.smtp_host');
        $mailer->Port = $config->get('mailer.smtp_port');
        $mailer->SMTPAuth = $useAuth;

        if ($useAuth) {
            $mailer->Username = $config->get('mailer.username');
            $mailer->Password = $config->get('mailer.password');
        }

        if (!$devMode) {
            $mailer->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
        }

        $mailer->isSMTP();
        $mailer->setFrom($config->get('mailer.from'));

        return $mailer;
    },
];
