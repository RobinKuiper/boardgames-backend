<?php

declare(strict_types = 1);

namespace App;

use App\Enum\AppEnvironment;

$boolean = function (mixed $value) {
    if (in_array($value, ['true', 1, '1', true, 'yes'], true)) {
        return true;
    }

    return false;
};

$appEnv = $_ENV['APP_ENV'] ?? AppEnvironment::Development->value;

return [
    'app_name'              => $_ENV['APP_NAME'] ?? 'Deployment Manager',
    'app_version'           => $_ENV['APP_VERSION'] ?? '1.0',
    'app_url'               => $_ENV['APP_URL'],
    'environment'           => $appEnv,
    'debug'                 => $boolean($_ENV['APP_DEBUG'] ?? false),
    'display_error_details' => $boolean($_ENV['APP_DEBUG'] ?? false),
    'log_errors'            => true,
    'log_error_details'     => true,
    'doctrine'              => [
        'dev_mode'   => AppEnvironment::isDevelopment($appEnv),
        'cache_dir'  => STORAGE_PATH . '/cache/doctrine',
        'entity_dir' => [APP_PATH . '/Entity'],
        'connection' => [
            'driver'   => $_ENV['DB_DRIVER'] ?? 'pdo_mysql',
            'host'     => $_ENV['DB_HOST'] ?? 'localhost',
            'port'     => $_ENV['DB_PORT'] ?? DEFAULT_DB_PORT,
            'dbname'   => $_ENV['DB_DATABASE'],
            'user'     => $_ENV['DB_USER'],
            'password' => $_ENV['DB_PASS'],
        ],
    ],
    'mailer'                => [
        'dev_mode'  => AppEnvironment::isDevelopment($appEnv),
        'from'      => $_ENV['MAILER_FROM'] ?? 'test@example.com',
        'smtp_host' => $_ENV['MAILER_SMTP_HOST'],
        'smtp_port' => $_ENV['MAILER_SMTP_PORT'] ?? DEFAULT_SMTP_PORT,
        'use_auth'  => $boolean($_ENV['MAILER_USE_AUTHENTICATION']),
        'username'  => $_ENV['MAILER_USERNAME'],
        'password'  => $_ENV['MAILER_PASSWORD'],
    ],
    'cors'                   => [
        'origin' => $_ENV['ALLOWED_ORIGIN'],
    ],
    'jwt_authentication'     => [
        'secret' => $_ENV['JWT_SECRET'],
        'algorithm' => 'HS256',
        'expiration_time' => 15 * 60, // 15 minutes in seconds
        'cookie_expiration_time' => 365 * 60 * 60 // 1 year in seconds
    ],
    'trusted_proxies'        => [],
];
