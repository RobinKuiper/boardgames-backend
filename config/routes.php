<?php

declare(strict_types = 1);

use App\Controllers\AuthController;
use App\Middleware\AuthMiddleware;
use App\Middleware\DataMiddleware;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
use Slim\Exception\HttpNotFoundException;
use Slim\Routing\RouteCollectorProxy;

return function (App $app) {
    $app->get('/', function (Response $response) {
        $response->getBody()->write('Nothing here!');

        return $response;
    });

    $app->group('/api', function (RouteCollectorProxy $api) {
        $api->group('/v1', function (RouteCollectorProxy $v1) {
            $v1->group('/auth', function (RouteCollectorProxy $auth) {
                $auth->post('/login', [AuthController::class, 'logIn'])->add(DataMiddleware::class);
                $auth->post('/token-refresh', [AuthController::class, 'tokenRenewal'])
                    ->add(AuthMiddleware::class)->setName('tokenRenewal');
            });
        });
    });

    $app->group('/api', function (RouteCollectorProxy $api) {
        $api->group('/v1', function (RouteCollectorProxy $v1) {
            foreach ((array)glob(CONFIG_PATH . '/routes/v1/*.php') as $file) {
                $routeFile = require $file;
                $routeFile($v1);
            }
        });
    })->add(AuthMiddleware::class);

    $app->options('/{routes:.+}', function (Response $response) {
        return $response;
    });

    $app->map(
        ['GET', 'POST', 'PUT', 'DELETE', 'PATCH'],
        '/{routes:.+}',
        function (Request $request) {
            throw new HttpNotFoundException($request);
        }
    );
};
