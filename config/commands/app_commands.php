<?php

declare(strict_types = 1);

use App\Command\AddAdminUser;
use App\Command\AddUser;
use App\Command\Deploy;
use App\Command\GenerateJwtSecretKeyCommand;
use App\Command\Setup;

return [
    AddAdminUser::class,
    AddUser::class,
    GenerateJwtSecretKeyCommand::class,
    Setup::class,
    Deploy::class,
];
