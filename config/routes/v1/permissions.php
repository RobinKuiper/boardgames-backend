<?php

use App\Controllers\PermissionController;
use App\Middleware\DataMiddleware;
use App\Middleware\PermissionMiddleware;
use Slim\Routing\RouteCollectorProxy;

return function (RouteCollectorProxy $group) {
    $group->group('/permissions', function (RouteCollectorProxy $permissions) {
        $permissions->get('', [PermissionController::class, 'getAll'])
            ->add(new PermissionMiddleware(['permission.get.all']));
        $permissions->post('', [PermissionController::class, 'create'])->add(DataMiddleware::class)
            ->add(new PermissionMiddleware(['permission.create']));

        $permissions->group('/{permission}', function (RouteCollectorProxy $permission) {
            $permission->get('', [PermissionController::class, 'get'])
                ->add(new PermissionMiddleware(['permission.get.all']));
            $permission->get('/logs', [PermissionController::class, 'getLogs'])
                ->add(new PermissionMiddleware(['log.get.all']));
            $permission->delete('', [PermissionController::class, 'delete'])
                ->add(new PermissionMiddleware(['permission.delete']));

            $permission->group('', function (RouteCollectorProxy $permissionWithData) {
                $permissionWithData->put('', [PermissionController::class, 'update'])
                  ->add(new PermissionMiddleware(['permission.update']));
                $permissionWithData->patch('', [PermissionController::class, 'patch'])
                  ->add(new PermissionMiddleware(['permission.update']));
            })->add(DataMiddleware::class);
        });
    });
};
