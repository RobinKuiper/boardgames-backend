<?php

use App\Controllers\UserProviderController;
use App\Middleware\DataMiddleware;
use App\Middleware\PermissionMiddleware;
use Slim\Routing\RouteCollectorProxy;

return function (RouteCollectorProxy $group) {
    $group->group('/users', function (RouteCollectorProxy $users) {
        $users->get('', [UserProviderController::class, 'getAll'])
            ->add(new PermissionMiddleware(['user.get.all']));
        $users->post('', [UserProviderController::class, 'create'])->add(DataMiddleware::class)
            ->add(new PermissionMiddleware(['user.create']));

        $users->group('/{user}', function (RouteCollectorProxy $user) {
            $user->get('', [UserProviderController::class, 'get'])
                ->add(new PermissionMiddleware(['user.get.all']));
            $user->get('/logs', [UserProviderController::class, 'getLogs'])
                ->add(new PermissionMiddleware(['log.get.all']));
            $user->delete('', [UserProviderController::class, 'delete'])
                ->add(new PermissionMiddleware(['user.delete']));

            $user->group('/permission', function (RouteCollectorProxy $permission) {
                $permission->post('', [UserProviderController::class, 'addPermission'])->add(DataMiddleware::class)
                  ->add(new PermissionMiddleware(['user.create'])); // TODO: Check permission
                $permission->delete('/{permission}', [UserProviderController::class, 'removePermission'])
                  ->add(new PermissionMiddleware(['user.create'])); // TODO: Check permission
            });
        });
    });
};
