<?php

declare(strict_types = 1);

use App\Controllers\GameController;
use App\Middleware\DataMiddleware;
use App\Middleware\PermissionMiddleware;
use Slim\Routing\RouteCollectorProxy;

return function (RouteCollectorProxy $group) {
    $group->group('/games', function (RouteCollectorProxy $games) {
        $games->get('', [GameController::class, 'getAll'])
            ->add(new PermissionMiddleware(['game.get.all']));
        $games->get('/my', [GameController::class, 'getMine'])
            ->add(new PermissionMiddleware(['game.get.all']));
        $games->get('/ours', [GameController::class, 'getOurs'])
            ->add(new PermissionMiddleware(['game.get.all']));
        $games->post('', [GameController::class, 'create'])->add(DataMiddleware::class)
            ->add(new PermissionMiddleware(['game.create']));

        $games->group('/{game}', function (RouteCollectorProxy $game) {
            $game->get('', [GameController::class, 'get'])
                ->add(new PermissionMiddleware(['game.get.all']));
            $game->get('/logs', [GameController::class, 'getLogs'])
                ->add(new PermissionMiddleware(['log.get.all']));
            $game->patch('/add-user', [GameController::class, 'addUser'])
                ->add(new PermissionMiddleware(['game.update']));
            $game->delete('/remove-user/{user}', [GameController::class, 'removeUser'])
                ->add(new PermissionMiddleware(['game.update']));
            $game->delete('', [GameController::class, 'delete'])
                ->add(new PermissionMiddleware(['game.delete']));

            $game->group('', function (RouteCollectorProxy $gamesWithData) {
                $gamesWithData->put('', [GameController::class, 'update'])
                    ->add(new PermissionMiddleware(['game.update']));
                $gamesWithData->patch('', [GameController::class, 'patch'])
                  ->add(new PermissionMiddleware(['game.update']));
            })->add(DataMiddleware::class);
        });
    });
};
