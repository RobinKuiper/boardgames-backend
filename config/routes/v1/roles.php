<?php

use App\Controllers\RoleController;
use App\Middleware\DataMiddleware;
use App\Middleware\PermissionMiddleware;
use Slim\Routing\RouteCollectorProxy;

return function (RouteCollectorProxy $group) {
    $group->group('/roles', function (RouteCollectorProxy $roles) {
        $roles->get('', [RoleController::class, 'getAll'])
            ->add(new PermissionMiddleware(['role.get.all']));
        $roles->post('', [RoleController::class, 'create'])->add(DataMiddleware::class)
            ->add(new PermissionMiddleware(['role.create']));

        $roles->group('/{role}', function (RouteCollectorProxy $role) {
            $role->get('', [RoleController::class, 'get'])
                ->add(new PermissionMiddleware(['role.get.all']));
            $role->get('/logs', [RoleController::class, 'getLogs'])
                ->add(new PermissionMiddleware(['log.get.all']));
            $role->delete('', [RoleController::class, 'delete'])
                ->add(new PermissionMiddleware(['role.delete']));

            $role->group('', function (RouteCollectorProxy $roleWithData) {
                $roleWithData->put('', [RoleController::class, 'update'])
                  ->add(new PermissionMiddleware(['role.update']));
                $roleWithData->patch('', [RoleController::class, 'patch'])
                  ->add(new PermissionMiddleware(['role.update']));
            })->add(DataMiddleware::class);

            $role->group('/permission', function (RouteCollectorProxy $permission) {
                $permission->post('', [RoleController::class, 'addPermission'])->add(DataMiddleware::class)
                  ->add(new PermissionMiddleware(['permission.create'])); // TODO: Check permission
                $permission->delete('/{permission}', [RoleController::class, 'removePermission'])
                  ->add(new PermissionMiddleware(['permission.delete'])); // TODO: Check permission
            });
        });
    });
};
