<?php

declare(strict_types = 1);

use App\Controllers\LogEntryController;
use App\Middleware\PermissionMiddleware;
use Slim\Routing\RouteCollectorProxy;

return function (RouteCollectorProxy $group) {
    $group->group('/log-entries', function (RouteCollectorProxy $logEntries) {
        $logEntries->get('', [LogEntryController::class, 'getAll'])
            ->add(new PermissionMiddleware(['log.get.all']));

        $logEntries->group('/{logEntry}', function (RouteCollectorProxy $logEntry) {
            $logEntry->get('', [LogEntryController::class, 'get'])
                ->add(new PermissionMiddleware(['log.get.all']));
        });
    });
};
