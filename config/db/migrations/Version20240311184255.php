<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240311184255 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add games from the BoardGameGeek csv into the database';
    }

    public function up(Schema $schema): void
    {
        $fields = ["id", "name", "year", "rank", "bayesaverage", "average"];

        if (($handle = fopen(__DIR__ . "/games.csv", "r")) !== false) {
            var_dump($handle);
            while (($data = fgetcsv($handle, 1000, ",")) !== false) {
                $numFields = count($data);
                $game = [];
                for ($c = 0; $c < $numFields; $c++) {
                    if (isset($fields[$c])) {
                        $game[$fields[$c]] = $data[$c];
                    }
                }
                ['name' => $name, 'year' => $year, 'id' => $bgg_id, 'rank' => $bgg_rank, 'average' => $bgg_average] = $game;
                if ($name !== 'name') {
                    $name = addslashes($name);
                    echo "Creating: " . $name . " | " . $year . " | " . $bgg_id . "\n";
                    $date = date('Y-m-d H:i:s');
                    $this->addSql("INSERT INTO games (name, year, bbg_id, bbg_rank, bbg_average, created_at, updated_at) VALUES (\"$name\", $year, $bgg_id, $bgg_rank, $bgg_average, \"$date\", \"$date\")");
                }
            }
            fclose($handle);
        }
    }

    public function down(Schema $schema): void
    {
        $this->addSql("TRUNCATE TABLE games");
    }
}
