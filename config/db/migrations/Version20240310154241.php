<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240310154241 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Seed with initial data';
    }

    public function up(Schema $schema): void
    {
        $date = date('Y-m-d H:i:s');
        $this->addDefaultRoles($date);
        $this->addDefaultPermissions($date);
        $this->addDefaultRolePermissions();
    }

    private function addDefaultRoles(string $date): void
    {
        $defaultRoles = ['User', 'Admin'];

        foreach ($defaultRoles as $name) {
            $this->addSql("INSERT INTO roles (name, created_at, updated_at) VALUES ('$name', '$date', '$date')");
        }
    }

    private function addDefaultPermissions(string $date): void
    {
        $defaultPermissions = [
            ['name' => 'event.get.all', 'description' => 'Get all events'],
            ['name' => 'event.create', 'description' => 'Create events'],
            ['name' => 'event.update', 'description' => 'Update events'],
            ['name' => 'event.delete', 'description' => 'Delete events'],

            ['name' => 'game.get.all', 'description' => 'Get all games'],
            ['name' => 'game.create', 'description' => 'Create games'],
            ['name' => 'game.update', 'description' => 'Update games'],
            ['name' => 'game.delete', 'description' => 'Delete games'],

            ['name' => 'user.get.all', 'description' => 'Get all users'],
            ['name' => 'user.create', 'description' => 'Create users'],
            ['name' => 'user.update', 'description' => 'Update users'],
            ['name' => 'user.delete', 'description' => 'Delete users'],

            ['name' => 'role.get.all', 'description' => 'Get all roles'],
            ['name' => 'role.create', 'description' => 'Create roles'],
            ['name' => 'role.update', 'description' => 'Update roles'],
            ['name' => 'role.delete', 'description' => 'Delete roles'],

            ['name' => 'permission.get.all', 'description' => 'Get all permissions'],
            ['name' => 'permission.create', 'description' => 'Create permissions'],
            ['name' => 'permission.update', 'description' => 'Update permissions'],
            ['name' => 'permission.delete', 'description' => 'Delete permissions'],

            ['name' => 'log.get.all', 'description' => 'Get all logs'],
        ];

        foreach ($defaultPermissions as $permission) {
            ['name' => $name, 'description' => $description] = $permission;
            $this->addSql("INSERT INTO permissions (name, description, created_at, updated_at, created_by, updated_by) VALUES ('$name', '$description', '$date', '$date', null, null)");
        }
    }

    private function addDefaultRolePermissions(): void
    {
        $defaultRolePermissions = [
            // Insert permissions for "User"
            ['roleId' => 1, 'permissionId' => 1],
            ['roleId' => 1, 'permissionId' => 2],
            ['roleId' => 1, 'permissionId' => 3],
            ['roleId' => 1, 'permissionId' => 4],

            // Insert permissions for "Admin"
            ['roleId' => 2, 'permissionId' => 1],
            ['roleId' => 2, 'permissionId' => 2],
            ['roleId' => 2, 'permissionId' => 3],
            ['roleId' => 2, 'permissionId' => 4],
            ['roleId' => 2, 'permissionId' => 5],
            ['roleId' => 2, 'permissionId' => 6],
            ['roleId' => 2, 'permissionId' => 7],
            ['roleId' => 2, 'permissionId' => 8],
            ['roleId' => 2, 'permissionId' => 9],
            ['roleId' => 2, 'permissionId' => 10],
            ['roleId' => 2, 'permissionId' => 11],
            ['roleId' => 2, 'permissionId' => 12],
            ['roleId' => 2, 'permissionId' => 13],
            ['roleId' => 2, 'permissionId' => 14],
            ['roleId' => 2, 'permissionId' => 15],
            ['roleId' => 2, 'permissionId' => 16],
            ['roleId' => 2, 'permissionId' => 17],
        ];

        foreach ($defaultRolePermissions as $rolePermission) {
            ['roleId' => $roleId, 'permissionId' => $permissionId] = $rolePermission;
            $this->addSql("INSERT INTO role_permission (role_id, permission_id) VALUES ($roleId, $permissionId)");
        }
    }

    public function down(Schema $schema): void
    {

    }
}
