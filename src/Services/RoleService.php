<?php

declare(strict_types = 1);

namespace App\Services;

use App\DataObjects\RoleData;
use App\Entity\Permission;
use App\Entity\Role;

class RoleService extends BaseService
{
    /**
     * Creates a new product based on the provided data.
     *
     * @param RoleData $data the data used to create the product
     *
     * @return Role the created product
     */
    public function create(RoleData $data): Role
    {
        $role = new Role();

        return $this->update(
            $role,
            $data
        );
    }

    /**
     * Updates the details of a product based on the provided data.
     *
     * @param Role           $role the product to update
     * @param RoleData $data    the updated data for the product
     *
     * @return Role the updated product
     */
    public function update(Role $role, RoleData $data): Role
    {
        $role->setName($data->name);

        $this->save($role);

        return $role;
    }

    /**
     * Updates a role with the provided data.
     *
     * @param Role $role the role to be updated
     * @param array $data the data used to update the role
     *
     * @return Role the updated role
     */
    public function patch(Role $role, array $data): Role
    {
        if (isset($data['name'])) {
            $role->setName($data['name']);
        }

        $this->save($role);

        return $role;
    }

    /**
     * Add a permission to a role.
     *
     * @param Role $role the role to which the permission is being added
     * @param Permission $permission the permission being added to the role
     *
     * @return Role the updated role with the added permission
     */
    public function addPermission(Role $role, Permission $permission): Role
    {
        $role->addPermission($permission);

        $this->save($role);

        return $role;
    }

    /**
     * Removes the specified permission from the given role.
     *
     * @param Role $role the role to remove the permission from
     * @param Permission $permission the permission to be removed
     *
     * @return Role the role object after removing the permission
     */
    public function removePermission(Role $role, Permission $permission): Role
    {
        $role->removePermission($permission);

        $this->save($role);

        return $role;
    }
}
