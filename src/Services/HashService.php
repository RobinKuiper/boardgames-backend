<?php

declare(strict_types = 1);

namespace App\Services;

class HashService
{
    /**
     * Hashes the given password using PHP's password_hash function.
     *
     * @param string $password the password to be hashed
     *
     * @return string the hashed password
     */
    public function hashPassword(string $password): string
    {
        return password_hash($password, PASSWORD_BCRYPT, ['cost' => 12]);
    }

    /**
     * Generates a random password.
     *
     * @param int $length
     *
     * @return string the generated password
     */
    public function generatePassword(int $length = DEFAULT_PASSWORD_LENGTH): string
    {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()-_+={}[]\|:;"<>,.?/';

        $password = '';

        for ($i = 0; $i < $length; $i++) {
            $password .= $chars[rand(0, strlen($chars) - 1)];
        }

        return $password;
    }
}
