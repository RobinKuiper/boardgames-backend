<?php

declare(strict_types = 1);

namespace App\Services;

use App\DataObjects\PermissionData;
use App\Entity\Permission;

class PermissionService extends BaseService
{
    /**
     * Creates a new product based on the provided data.
     *
     * @param PermissionData $data the data used to create the product
     *
     * @return Permission the created product
     */
    public function create(PermissionData $data): Permission
    {
        $permission = new Permission();

        return $this->update(
            $permission,
            $data
        );
    }

    /**
     * Updates the details of a product based on the provided data.
     *
     * @param Permission           $permission the product to update
     * @param PermissionData $data    the updated data for the product
     *
     * @return Permission the updated product
     */
    public function update(Permission $permission, PermissionData $data): Permission
    {
        $permission->setName($data->name);
        $permission->setDescription($data->description);

        $this->save($permission);

        return $permission;
    }

    /**
     * Updates the fields of a given permission based on the provided data.
     *
     * @param Permission $permission the permission object to update
     * @param array $data the data used to update the permission
     *
     * @return Permission the updated permission object
     */
    public function patch(Permission $permission, array $data): Permission
    {
        if (isset($data['name'])) {
            $permission->setName($data['name']);
        }

        if (isset($data['description'])) {
            $permission->setDescription($data['description']);
        }

        $this->save($permission);

        return $permission;
    }
}
