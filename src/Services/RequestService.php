<?php

declare(strict_types = 1);

namespace App\Services;

use App\DataObjects\DataTableQueryParams;
use Psr\Http\Message\ServerRequestInterface;

class RequestService
{
    public function __construct()
    {
    }

    public function getReferer(ServerRequestInterface $request): string
    {
        return $request->getHeader('referer')[0] ?? '';
    }

    public function isXhr(ServerRequestInterface $request): bool
    {
        return $request->getHeaderLine('X-Requested-With') === 'XMLHttpRequest';
    }

    public function isApi(ServerRequestInterface $request): bool
    {
        return str_contains($request->getRequestTarget(), 'api');
    }

    public function getDataTableQueryParameters(ServerRequestInterface $request): DataTableQueryParams
    {
        $params   = $request->getQueryParams();

        return new DataTableQueryParams(
            array_key_exists('start', $params) ? (int)$params['start'] : 0,
            array_key_exists('length', $params) ? (int)$params['length'] : 100,
            $params['orderBy'] ?? '',
            $params['orderDir'] ?? '',
            $params['query'] ?? '',
            1
        );
    }

    public function getClientIp(ServerRequestInterface $request, array $trustedProxies): ?string
    {
        $serverParams = $request->getServerParams();

        if (in_array($serverParams['REMOTE_ADDR'], $trustedProxies, true)
            && isset($serverParams['HTTP_X_FORWARDED_FOR'])) {
            $ips = explode(',', $serverParams['HTTP_X_FORWARDED_FOR']);

            return trim($ips[0]);
        }

        return $serverParams['REMOTE_ADDR'] ?? null;
    }
}
