<?php

declare(strict_types = 1);

namespace App\Services;

use App\Interfaces\EntityManagerServiceInterface;

class BaseService
{
    public function __construct(protected EntityManagerServiceInterface $entityManager)
    {
    }

    protected function save(object $entity): void
    {
        $this->entityManager->sync($entity);
    }
}
