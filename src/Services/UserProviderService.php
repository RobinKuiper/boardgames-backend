<?php

declare(strict_types = 1);

namespace App\Services;

use App\DataObjects\RegisterUserData;
use App\Entity\Permission;
use App\Entity\User;
use App\Interfaces\EntityManagerServiceInterface;
use App\Interfaces\UserInterface;
use App\Interfaces\UserProviderServiceInterface;

readonly class UserProviderService implements UserProviderServiceInterface
{
    public function __construct(
        private EntityManagerServiceInterface $entityManager,
        private HashService $hashService
    ) {
    }

    /**
     * Get user by id.
     *
     * @param int $userId the user id
     *
     * @return UserInterface|null the user object if found, null otherwise
     */
    public function getById(int $userId): ?UserInterface
    {
        return $this->entityManager->find(User::class, $userId);
    }

    /**
     * Retrieves a user by their credentials.
     *
     * @param array $credentials an array containing the user's credentials, such as email
     *
     * @return UserInterface|null the user object if found, or null if not found
     */
    public function getByCredentials(array $credentials): ?UserInterface
    {
        return $this->entityManager->getRepository(User::class)->findOneBy(['email' => $credentials['email']]);
    }

    /**
     * Creates a new User object based on the provided data.
     *
     * @param RegisterUserData $data the registration data for the new user
     *
     * @return UserInterface the newly created User object
     */
    public function createUser(RegisterUserData $data): UserInterface
    {
        $user = new User();

        $user->setName($data->name);
        $user->setEmail($data->email);
        $user->setPassword($this->hashService->hashPassword($data->password));
        $user->setRole($data->role);

        $this->entityManager->sync($user);

        return $user;
    }

    /**
     * Updates the password for a user.
     *
     * @param UserInterface $user the user object to update the password for
     * @param string $password the new password string
     *
     * @return UserInterface the updated user object
     */
    public function updatePassword(UserInterface $user, string $password): UserInterface
    {
        $user->setPassword($this->hashService->hashPassword($password));

        $this->entityManager->sync($user);

        return $user;
    }

    /**
     * Adds a permission to a user.
     *
     * @param UserInterface $user the user object to add the permission to
     * @param Permission $permission the permission object to be added
     *
     * @return UserInterface the updated user object with the added permission
     */
    public function addPermission(UserInterface $user, Permission $permission): UserInterface
    {
        $user->addPermission($permission);

        $this->entityManager->sync($user);

        return $user;
    }

    /**
     * Removes a permission from a user.
     *
     * @param UserInterface $user the user object from which the permission will be removed
     * @param Permission $permission the permission object to be removed
     *
     * @return UserInterface the updated user object
     */
    public function removePermission(UserInterface $user, Permission $permission): UserInterface
    {
        $user->removePermission($permission);

        $this->entityManager->sync($user);

        return $user;
    }
}
