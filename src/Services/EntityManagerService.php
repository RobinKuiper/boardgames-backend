<?php

declare(strict_types = 1);

namespace App\Services;

use App\Interfaces\EntityManagerServiceInterface;
use BadMethodCallException;
use Doctrine\ORM\EntityManagerInterface;

readonly class EntityManagerService implements EntityManagerServiceInterface
{
    public function __construct(protected EntityManagerInterface $entityManager)
    {
    }

    /**
     * Magic method to handle calls to undefined methods.
     *
     * @param string $name the name of the method being called
     * @param array $arguments the arguments passed to the method
     *
     * @throws BadMethodCallException if the method does not exist
     *
     * @return mixed the result of the method call
     */
    public function __call(string $name, array $arguments): mixed
    {
        if (method_exists($this->entityManager, $name)) {
            $callable = [$this->entityManager, $name];
            assert(is_callable($callable));

            return call_user_func_array($callable, $arguments);
        }

        throw new BadMethodCallException('Call to undefined method "' . $name . '"');
    }

    /**
     * Synchronizes an entity with the database.
     *
     * @param object|null $entity The entity to synchronize. If null, no entity will be synchronized.
     *
     * @return void
     */
    public function sync(?object $entity = null): void
    {
        if ($entity) {
            $this->entityManager->persist($entity);
        }

        $this->entityManager->flush();
    }

    /**
     * Deletes an entity from the entity manager.
     *
     * @param object $entity the entity to delete
     * @param bool $sync Optional. Whether to sync the changes immediately. Default is false.
     *
     * @return void
     */
    public function delete(object $entity, bool $sync = false): void
    {
        $this->entityManager->remove($entity);

        if ($sync) {
            $this->sync();
        }
    }

    /**
     * Clears the entity manager of all entities or of entities of a specific type.
     *
     * @param string|null $entityName The name of the entity type. If null, all entities will be cleared.
     *
     * @return void
     */
    public function clear(?string $entityName = null): void
    {
        if ($entityName === null) {
            $this->entityManager->clear();

            return;
        }

        $unitOfWork = $this->entityManager->getUnitOfWork();
        $entities = $unitOfWork->getIdentityMap()[$entityName] ?? [];

        foreach ($entities as $entity) {
            $this->entityManager->detach($entity);
        }
    }
}
