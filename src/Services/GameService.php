<?php

declare(strict_types = 1);

namespace App\Services;

use App\DataObjects\DataTableQueryParams;
use App\DataObjects\GameData;
use App\Entity\Game;
use App\Entity\User;
use Doctrine\ORM\Tools\Pagination\Paginator;

class GameService extends BaseService
{
    /**
     * Creates a new game based on the provided create data.
     *
     * @param GameData $data the data for creating the game
     *
     * @return Game the newly created game
     */
    public function create(GameData $data): Game
    {
        $game = new Game();

        return $this->update($game, $data);
    }

    /**
     * Updates the provided game with the given update data.
     *
     * @param Game           $game the game to be updated
     * @param GameData       $data   the data for updating the game
     *
     * @return Game the updated game
     */
    public function update(Game $game, GameData $data): Game
    {
        $game->setName($data->name);
        $game->setYear($data->year);
        $game->setBoardGameGeekId($data->boardGameGeekId);

        $this->save($game);

        return $game;
    }

    /**
     * Updates a game based on the provided patch data.
     *
     * @param Game $game the game to be updated
     * @param array $data the patch data for updating the game
     *
     * @return Game the updated game
     */
    public function patch(Game $game, array $data): Game
    {
        if (isset($data['name'])) {
            $game->setName($data['name']);
        }

        if (isset($data['year'])) {
            $game->setYear($data['year']);
        }

        if (isset($data['boardGameGeekId'])) {
            $game->setBoardGameGeekId($data['boardGameGeekId']);
        }

        $this->save($game);

        return $game;
    }

    public function addUser(Game $game, User $user): Game
    {
        $user->addGame($game);

        $this->save($user);

        return $game;
    }

    public function removeUser(Game $game, User $user): Game
    {
        $game->removeUser($user);

        $this->save($game);

        return $game;
    }

    /**
     * Retrieves paginated results based on the provided DataTableQueryParams.
     *
     * @param DataTableQueryParams $params The query parameters for pagination.
     * @return Paginator The paginated results.
     */
    public function getPaginatedResults(DataTableQueryParams $params): Paginator
    {
        $query = $this->entityManager
            ->getRepository(Game::class)
            ->createQueryBuilder('g')
            ->setFirstResult($params->start)
            ->setMaxResults($params->length);

        $orderBy  = in_array($params->orderBy, ['name', 'year', 'boardGameGeekRank', 'createdAt', 'updatedAt']) ? $params->orderBy : 'id';
        $orderDir = strtolower($params->orderDir) === 'asc' ? 'asc' : 'desc';

        if (! empty($params->searchTerm)) {
            $query->where('g.name LIKE :name')->setParameter(
                'name',
                '%' . addcslashes($params->searchTerm, '%_') . '%'
            );
            $query->orWhere('g.boardGameGeekId LIKE :id')->setParameter(
                'id',
                '%' . addcslashes($params->searchTerm, '%_') . '%'
            );
            $query->orWhere('g.year LIKE :year')->setParameter(
                'year',
                '%' . addcslashes($params->searchTerm, '%_') . '%'
            );
        }

        $query->orderBy('g.' . $orderBy, $orderDir);

        return new Paginator($query);
    }
}
