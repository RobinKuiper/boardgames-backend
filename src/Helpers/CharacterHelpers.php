<?php

namespace App\Helpers;

use App\Entity\Action;
use App\Entity\Character;
use App\Enum\ActionGroup;
use DI\NotFoundException;

class CharacterHelpers {
    /**
     * @throws NotFoundException
     */
    static public function calculateModifiers(Character $character, Action $action): array
    {
        $jsonPath   = STORAGE_PATH . '/milestones.json';
        $jsonData   = file_get_contents($jsonPath);

        if (!$jsonData) {
            throw new NotFoundException("milestones.json file not found in storage");
        }

        $milestones = json_decode($jsonData, true);

        $experience = CharacterHelpers::getRelativeSkillExperience($character, $action);
        $level      = LevelHelpers::calculateLevelByExperience($experience);

        $milestoneGroup     = $milestones[$action->getActionGroup()->name];
        $obtainedMilestones = array_filter($milestoneGroup, fn (array $milestone) => $milestone['level'] <= $level);

        $modifiers = [
            "drops" => 0,
            "experience" => 0
        ];

        foreach ($obtainedMilestones as $milestone) {
            foreach ($milestone['modifiers'] as $modifier) {
                switch ($modifier['group']) {
                    case 'drop':
                        $modifiers['drops'] += $modifier['amount'];
                        break;

                    case 'experience':
                        $modifiers['experience'] += $modifier['amount'];
                        break;
                }
            }
        }

        return $modifiers;
    }

    static private function getRelativeSkillExperience(Character $character, Action $action): float
    {
        return match ($action->getActionGroup()) {
            ActionGroup::Mining => $character->getCharacterSkills()->getMining(),
            ActionGroup::Woodcutting => $character->getCharacterSkills()->getWoodcutting(),
        };
    }
}