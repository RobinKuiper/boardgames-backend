<?php

namespace App\Helpers;

class LevelHelpers {
    static function calculateExperienceByLevel(int $level): float
    {
        $experience = 0;

        for ($ell = 1; $ell < $level; $ell++) {
            $innerSum = floor($ell + 300 * 2 ** ($ell / 7));
            $experience += $innerSum;
        }

        return floor($experience / 4);
    }

    static function calculateExperienceDifferenceByLevel(int $level): float
    {
        return floor(1/4 * ($level - 1 + 300 * 2 ** (($level - 1) / 7)));
    }

    static function calculateLevelByExperience(float $targetExperience): int
    {
        $experience = 0;
        $level = 1;

        while ($experience <= $targetExperience) {
            $experience = LevelHelpers::calculateExperienceByLevel($level);
            $level++;
        }

        return $level - 2; // Subtracting 2 to get the level before the experience exceeds the target
    }
}