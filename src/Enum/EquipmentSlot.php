<?php

declare(strict_types = 1);

namespace App\Enum;

enum EquipmentSlot: string
{
    case Weapon1 = 'Weapon1';
    case Weapon2      = 'Weapon2';
    case Head       = 'Head';
    case Body       = 'Body';
    case Hands       = 'Hands';
    case Legs       = 'Legs';
    case Feet       = 'Feet';
    case Ring1       = 'Ring1';
    case Ring2       = 'Ring2';
    case Necklace       = 'Necklace';
}
