<?php

declare(strict_types = 1);

namespace App\Enum;

enum ActionGroup: string
{
    case Woodcutting = 'Woodcutting';
    case Mining      = 'Mining';
    case Village      = 'Village';
}
