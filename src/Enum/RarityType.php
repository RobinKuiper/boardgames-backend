<?php

declare(strict_types = 1);

namespace App\Enum;

enum RarityType: string
{
    case Common = 'Common';
    case Rare      = 'Rare';
    case Epic       = 'Epic';
    case Legendary       = 'Legendary';
}
