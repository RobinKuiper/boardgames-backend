<?php

declare(strict_types = 1);

namespace App\DataObjects;

use App\Entity\Role;

readonly class RegisterUserData
{
    public function __construct(
        public string $name,
        public string $email,
        public string $password,
        public ?Role $role,
    ) {
    }
}
