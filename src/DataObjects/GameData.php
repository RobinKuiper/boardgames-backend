<?php

declare(strict_types = 1);

namespace App\DataObjects;

readonly class GameData
{
    public function __construct(
        public string    $name,
        public int       $year,
        public ?int      $boardGameGeekId,
    ) {
    }
}
