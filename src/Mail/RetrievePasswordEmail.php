<?php

declare(strict_types = 1);

namespace App\Mail;

use App\Entity\User;
use App\Mailer;
use PHPMailer\PHPMailer\Exception;

readonly class RetrievePasswordEmail
{
    public function __construct(private Mailer $mailer)
    {
    }

    /**
     * Sends a password mail to a user.
     *
     * @param User $user The user to send the password mail to
     * @param string $password The password to include in the mail body
     *
     * @throws Exception
     *
     * @return void
     */
    public function send(User $user, string $password): void
    {
        $this->mailer->isHTML(false);
        $this->mailer->addAddress($user->getEmail());
        $this->mailer->Subject = 'Password mail';
        $this->mailer->Body = $password;

        $this->mailer->send();
    }
}
