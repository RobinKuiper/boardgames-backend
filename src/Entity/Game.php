<?php

declare(strict_types = 1);

namespace App\Entity;

use App\Entity\Traits\HasTimestamps;
use App\Entity\Traits\IsBlameable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\Table;
use Gedmo\Loggable\Loggable;
use Gedmo\Mapping\Annotation as Gedmo;
use JsonSerializable;

#[Entity, Table('games')]
#[HasLifecycleCallbacks]
#[Gedmo\Loggable(logEntryClass: LogEntry::class)]
class Game implements JsonSerializable, Loggable
{
    use HasTimestamps;
    use isBlameable;

    #[Id, Column(options: ['unsigned' => true]), GeneratedValue]
    private int $id;

    #[Gedmo\Versioned]
    #[Column]
    private string $name;

    #[Gedmo\Versioned]
    #[Column]
    private int $year;

    #[Gedmo\Versioned]
    #[Column(name: "bbg_id", unique: true, nullable: true)]
    private ?int $boardGameGeekId = null;

    #[Gedmo\Versioned]
    #[Column(name: "bbg_rank", nullable: true)]
    private ?int $boardGameGeekRank = null;

    #[Gedmo\Versioned]
    #[Column(name: "bbg_average", nullable: true)]
    private ?float $boardGameGeekAverage = null;

    /**
     * @var Collection<int, User> $users
     */
    #[ManyToMany(targetEntity: User::class, mappedBy: 'games')]
    private Collection $users;

    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getYear(): int
    {
        return $this->year;
    }

    public function setYear(int $year): void
    {
        $this->year = $year;
    }

    public function getBoardGameGeekId(): int|null
    {
        return $this->boardGameGeekId;
    }

    public function setBoardGameGeekId(?int $boardGameGeekId): void
    {
        $this->boardGameGeekId = $boardGameGeekId;
    }

    public function getBoardGameGeekRank(): int|null
    {
        return $this->boardGameGeekRank;
    }

    public function setBoardGameGeekRank(?int $boardGameGeekRank): void
    {
        $this->boardGameGeekRank = $boardGameGeekRank;
    }

    public function getBoardGameGeekAverage(): float|null
    {
        return $this->boardGameGeekAverage;
    }

    public function setBoardGameGeekAverage(?float $boardGameGeekAverage): void
    {
        $this->boardGameGeekAverage = $boardGameGeekAverage;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): void
    {
        $this->users[] = $user;
    }

    public function removeUser(User $user): void
    {
        $this->users->removeElement($user);
    }

    public function jsonSerialize(): array
    {
        return [
            'id'                   => $this->id,
            'name'                 => $this->name,
            'year'                 => $this->year,
            'boardGameGeekId'      => $this->boardGameGeekId,
            'boardGameGeekRank'    => $this->boardGameGeekRank,
            'boardGameGeekAverage' => $this->boardGameGeekAverage,
//            'users' => $this->users->map(fn (User $user) => [
//                'id'          => $user->getId(),
//                'name'        => $user->getName(),
//                'createdAt'   => $user->getCreatedAt(),
//                'updatedAt'   => $user->getUpdatedAt(),
//                'createdBy'   => $user->getCreatedBy(),
//                'updatedBy'   => $user->getUpdatedBy(),
//            ])->toArray(),
            'createdAt'            => $this->createdAt,
            'updatedAt'            => $this->updatedAt,
            'createdBy'            => $this->createdBy,
            'updatedBy'            => $this->updatedBy,
        ];
    }
}
