<?php

declare(strict_types = 1);

namespace App\Entity;

use App\Entity\Traits\HasTimestamps;
use App\Entity\Traits\IsBlameable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\Table;
use Gedmo\Loggable\Loggable;
use Gedmo\Mapping\Annotation as Gedmo;
use JsonSerializable;

#[Entity, Table('permissions')]
#[HasLifecycleCallbacks]
#[Gedmo\Loggable(logEntryClass: LogEntry::class)]
class Permission implements JsonSerializable, Loggable
{
    use HasTimestamps;
    use isBlameable;

    #[Id, Column(options: ['unsigned' => true]), GeneratedValue]
    private int $id;

    #[Gedmo\Versioned]
    #[Column]
    private string $name;

    #[Gedmo\Versioned]
    #[Column]
    private string $description;

    /**
     * @var Collection<int, Role> $roles
     */
    #[ManyToMany(targetEntity: Role::class, mappedBy: 'permissions')]
    private Collection $roles;

    /**
     * @var Collection<int, User> $users
     */
    #[ManyToMany(targetEntity: User::class, mappedBy: 'permissions')]
    private Collection $users;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->roles = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return Collection<int, Role>
     */
    public function getRoles(): Collection
    {
        return $this->roles;
    }

    public function addRole(Role $role): void
    {
        $this->roles[] = $role;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): void
    {
        $this->users[] = $user;
    }

    public function jsonSerialize(): array
    {
        return [
            'id'           => $this->id,
            'name'         => $this->name,
            'description'  => $this->description,
            'users'        => $this->users->map(fn (User $user) => [
                'id'        => $user->getId(),
                'name'      => $user->getName(),
                'email'     => $user->getEmail(),
                'role'      => [
                    'id'        => $user->getRole()->getId(),
                    'name'      => $user->getRole()->getName(),
                    'createdAt' => $user->getRole()->getCreatedAt(),
                    'updatedAt' => $user->getRole()->getUpdatedAt(),
                ],
                'permissions' => $user->getPermissions()->map(fn (Permission $permission) => [
                    'id'           => $permission->getId(),
                    'name'         => $permission->getName(),
                    'description'  => $permission->getDescription(),
                    'createdBy'    => $permission->getCreatedBy(),
                    'updatedBy'    => $permission->getUpdatedBy(),
                    'createdAt'    => $permission->getCreatedAt(),
                    'updatedAt'    => $permission->getUpdatedAt(),
                ])->toArray(),
                'createdBy' => $user->getCreatedBy(),
                'updatedBy' => $user->getUpdatedBy(),
                'createdAt' => $user->getCreatedAt(),
                'updatedAt' => $user->getUpdatedAt(),
            ])->toArray(),
            'roles'        => $this->roles->map(fn (Role $role) => [
                'id'        => $role->getId(),
                'name'      => $role->getName(),
                'users'     => $role->getUsers()->map(fn (User $user) => [
                    'id'        => $user->getId(),
                    'name'      => $user->getName(),
                    'email'     => $user->getEmail(),
                    'createdBy' => $user->getCreatedBy(),
                    'updatedBy' => $user->getUpdatedBy(),
                    'createdAt' => $user->getCreatedAt(),
                    'updatedAt' => $user->getUpdatedAt(),
                ])->toArray(),
                'permissions' => $role->getPermissions()->map(fn (Permission $permission) => [
                    'id'           => $permission->getId(),
                    'name'         => $permission->getName(),
                    'description'  => $permission->getDescription(),
                    'createdBy'    => $permission->getCreatedBy(),
                    'updatedBy'    => $permission->getUpdatedBy(),
                    'createdAt'    => $permission->getCreatedAt(),
                    'updatedAt'    => $permission->getUpdatedAt(),
                ])->toArray(),
                'createdAt' => $role->getCreatedAt(),
                'updatedAt' => $role->getUpdatedAt(),
            ])->toArray(),
            'createdAt'    => $this->createdAt,
            'updatedAt'    => $this->updatedAt,
            'createdBy'    => $this->createdBy,
            'updatedBy'    => $this->updatedBy,
        ];
    }
}
