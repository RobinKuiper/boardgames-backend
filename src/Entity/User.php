<?php

declare(strict_types = 1);

namespace App\Entity;

use App\Entity\Traits\HasTimestamps;
use App\Entity\Traits\IsBlameable;
use App\Interfaces\UserInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\Table;
use Gedmo\Loggable\Loggable;
use Gedmo\Mapping\Annotation as Gedmo;
use JsonSerializable;

#[Entity, Table('users')]
#[HasLifecycleCallbacks]
#[Gedmo\Loggable(logEntryClass: LogEntry::class)]
class User implements UserInterface, JsonSerializable, Loggable
{
    use HasTimestamps;
    use isBlameable;

    #[Id, Column(options: ['unsigned' => true]), GeneratedValue]
    private int $id;

    #[Gedmo\Versioned]
    #[Column]
    private string $name;

    #[Gedmo\Versioned]
    #[Column]
    private string $email;

    #[Gedmo\Versioned]
    #[Column]
    private string $password;

    #[Gedmo\Versioned]
    #[ManyToOne(targetEntity: Role::class, inversedBy: 'users')]
    private Role $role;

    /**
     * @var Collection<int, Permission> $permissions
     */
    #[ManyToMany(targetEntity: Permission::class, inversedBy: 'users')]
    private Collection $permissions;

    /**
     * @var Collection<int, Game> $games
     */
    #[ManyToMany(targetEntity: Game::class, inversedBy: 'users')]
    private Collection $games;

    public function __construct()
    {
        $this->permissions = new ArrayCollection();
        $this->games       = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): User
    {
        $this->name = $name;

        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): User
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): User
    {
        $this->password = $password;

        return $this;
    }

    public function getRole(): Role
    {
        return $this->role;
    }

    public function setRole(Role $role): void
    {
        $this->role = $role;
    }

    /**
     * @return Collection<int, Permission>
     */
    public function getPermissions(): Collection
    {
        return $this->permissions;
    }

    public function addPermission(Permission $permission): void
    {
        $this->permissions[] = $permission;
    }

    public function removePermission(Permission $permission): void
    {
        $this->permissions->removeElement($permission);
    }

    /**
     * @return Collection<int, Game>
     */
    public function getGames(): Collection
    {
        return $this->games;
    }

    public function addGame(Game $game): void
    {
        $this->games[] = $game;
    }

    public function removeGame(Game $game): void
    {
        $this->games->removeElement($game);
    }

    public function jsonSerialize(): array
    {
        return [
            'id'          => $this->id,
            'name'        => $this->name,
            'email'       => $this->email,
            'role'        => [
                'id'        => $this->role->getId(),
                'name'      => $this->role->getName(),
                //                "createdBy" => $this->role->getCreatedBy(),
                //                "updatedBy" => $this->role->getUpdatedBy(),
                'createdAt' => $this->role->getCreatedAt(),
                'updatedAt' => $this->role->getUpdatedAt(),
            ],
            'permissions' => $this->permissions->map(fn (Permission $permission) => [
                'id'           => $permission->getId(),
                'name'         => $permission->getName(),
                'description'  => $permission->getDescription(),
                'createdBy'    => $permission->getCreatedBy(),
                'updatedBy'    => $permission->getUpdatedBy(),
                'createdAt'    => $permission->getCreatedAt(),
                'updatedAt'    => $permission->getUpdatedAt(),
            ])->toArray(),
            'games'       => $this->games->map(fn (Game $game) => [
                'id'              => $game->getId(),
                'name'            => $game->getName(),
                'year'            => $game->getYear(),
                'boardGameGeekId' => $game->getBoardGameGeekId(),
                'createdBy'       => $game->getCreatedBy(),
                'updatedBy'       => $game->getUpdatedBy(),
                'createdAt'       => $game->getCreatedAt(),
                'updatedAt'       => $game->getUpdatedAt(),
            ])->toArray(),
            'createdAt'   => $this->createdAt,
            'updatedAt'   => $this->updatedAt,
        ];
    }
}
