<?php

declare(strict_types = 1);

namespace App\Entity;

use App\Entity\Traits\HasTimestamps;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\Table;
use Gedmo\Loggable\Loggable;
use Gedmo\Mapping\Annotation as Gedmo;
use JsonSerializable;

#[Entity, Table('roles')]
#[HasLifecycleCallbacks]
#[Gedmo\Loggable(logEntryClass: LogEntry::class)]
class Role implements JsonSerializable, Loggable
{
    use HasTimestamps;

    #[Id, Column(options: ['unsigned' => true]), GeneratedValue]
    private int $id;

    #[Gedmo\Versioned]
    #[Column]
    private string $name;

    /**
     * @var Collection<int, User> $users
     */
    #[OneToMany(mappedBy: 'role', targetEntity: User::class)]
    private Collection $users;

    /**
     * @var Collection<int, Permission> $permissions
     */
    #[ManyToMany(targetEntity: Permission::class, inversedBy: 'roles')]
    private Collection $permissions;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->permissions = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): void
    {
        $this->users[] = $user;
    }

    /**
     * @return Collection<int, Permission>
     */
    public function getPermissions(): Collection
    {
        return $this->permissions;
    }

    public function addPermission(Permission $permission): void
    {
        $this->permissions[] = $permission;
    }

    public function removePermission(Permission $permission): void
    {
        $this->permissions->removeElement($permission);
    }

    public function jsonSerialize(): array
    {
        return [
            'id'          => $this->id,
            'name'        => $this->name,
            'users'       => $this->users->map(fn (User $user) => [
                'id'        => $user->getId(),
                'name'      => $user->getName(),
                'email'     => $user->getEmail(),
                'createdBy' => $user->getCreatedBy(),
                'updatedBy' => $user->getUpdatedBy(),
                'createdAt' => $user->getCreatedAt(),
                'updatedAt' => $user->getUpdatedAt(),
            ])->toArray(),
            'permissions' => $this->permissions->map(fn (Permission $permission) => [
                'id'           => $permission->getId(),
                'name'         => $permission->getName(),
                'description'  => $permission->getDescription(),
                'createdBy'    => $permission->getCreatedBy(),
                'updatedBy'    => $permission->getUpdatedBy(),
                'createdAt'    => $permission->getCreatedAt(),
                'updatedAt'    => $permission->getUpdatedAt(),
            ])->toArray(),
            'createdAt'   => $this->createdAt,
            'updatedAt'   => $this->updatedAt,
            //            "createdBy"   => $this->createdBy,
            //            "updatedBy"   => $this->updatedBy,
        ];
    }
}
