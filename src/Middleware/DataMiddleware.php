<?php

declare(strict_types = 1);

namespace App\Middleware;

use App\Enum\StatusCode;
use App\ResponseFormatter;
use App\Services\RequestService;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class DataMiddleware implements MiddlewareInterface
{
    public function __construct(
        private readonly ResponseFactoryInterface $responseFactory,
        private readonly ResponseFormatter $responseFormatter,
        private readonly RequestService $requestService,
    ) {
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        if (is_null($request->getParsedBody())) {
            if ($this->requestService->isXhr($request) || $this->requestService->isApi($request)) {
                return $this->responseFormatter->asJson(
                    $this->responseFactory->createResponse()->withStatus(StatusCode::BadRequest->value),
                    [
                        'message' => 'Bad Request',
                        'errors'  => [
                            'No data was given',
                        ],
                    ]
                );
            }

            $referer  = $this->requestService->getReferer($request);

            return $this->responseFactory->createResponse(StatusCode::Found->value)
              ->withHeader('Location', $referer);
        }

        return $handler->handle($request);
    }
}
