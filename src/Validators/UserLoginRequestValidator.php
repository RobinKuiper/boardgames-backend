<?php

declare(strict_types = 1);

namespace App\Validators;

use App\Exceptions\ValidationException;
use App\Interfaces\RequestValidatorInterface;
use Valitron\Validator;

class UserLoginRequestValidator implements RequestValidatorInterface
{
    public function validate(array $data, ?string $method = null): array
    {
        $v = new Validator($data);

        $v->rule('required', ['email', 'password'])->message('Required field');
        $v->rule('email', 'email');

        if (!$v->validate() && is_array($v->errors())) {
            throw new ValidationException($v->errors());
        }

        return $data;
    }
}
