<?php

declare(strict_types = 1);

namespace App\Validators;

use App\Entity\Role;
use App\Interfaces\EntityManagerServiceInterface;
use App\Interfaces\RequestValidatorInterface;
use App\Entity\User;
use App\Exceptions\ValidationException;
use Valitron\Validator;

readonly class RegisterUserRequestValidator implements RequestValidatorInterface
{
    public function __construct(private EntityManagerServiceInterface $entityManager)
    {
    }

    public function validate(array $data, ?string $method = null): array
    {
        $v = new Validator($data);

        $data['role'] = $this->entityManager->find(Role::class, 1);

        $v->rule('required', ['name', 'email', 'password', 'confirmPassword'])->message('Required field');
        $v->rule('email', 'email');
        $v->rule('equals', 'confirmPassword', 'password')->label('Confirm Password');
        $v->rule(
            fn($field, $value, $params, $fields) => !$this->entityManager->getRepository(User::class)->count(
                ['email' => $value]
            ),
            'email'
        )->message('User with the given email address already exists');

        if (! $v->validate()) {
            throw new ValidationException($v->errors());
        }

        return $data;
    }
}