<?php

declare(strict_types = 1);

namespace App\Validators;

use App\Entity\Permission;
use App\Exceptions\ValidationException;
use App\Interfaces\EntityManagerServiceInterface;
use App\Interfaces\RequestValidatorInterface;
use Doctrine\ORM\EntityRepository;
use Valitron\Validator;

class AddPermissionRequestValidator implements RequestValidatorInterface
{
    /**
     * @var EntityRepository<Permission>
     */
    protected readonly EntityRepository $repository;

    public function __construct(
        protected EntityManagerServiceInterface $entityManagerService,
        protected Validator $validator
    ) {
        $objectRepository = $entityManagerService->getRepository(Permission::class);
        assert($objectRepository instanceof EntityRepository);
        $this->repository = $objectRepository;
    }

    public function validate(array $data, ?string $method = null): array
    {
        $v = $this->validator->withData($data);

        $data['permission'] = array_key_exists('permissionId', $data)
          ? $this->entityManagerService->getRepository(Permission::class)->find((int)$data['permissionId']) : null;

        $v->rule('required', ['permissionId']);

        $v->rule(
            function () use ($data) {
                return !is_null($data['permission']);
            },
            'permissionId'
        )->message('Permission with the given id does not exist');

        if (!$v->validate() && is_array($v->errors())) {
            throw new ValidationException($v->errors());
        }

        return $data;
    }
}
