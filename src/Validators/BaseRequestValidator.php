<?php

declare(strict_types = 1);

namespace App\Validators;

use App\Exceptions\ValidationException;
use App\Interfaces\RequestValidatorInterface;
use Valitron\Validator;

class BaseRequestValidator implements RequestValidatorInterface
{
    protected array $methodToRulesMap = [
        'POST' => 'setCreationRules',
        'PUT' => 'setCreationRules',
        'PATCH' => 'setPatchRules',
    ];

    public function __construct(
        protected Validator $validator
    ) {
    }

    public function validate(array $data, ?string $method = null): array
    {
        $v = $this->validator->withData($data);

        $ruleMethod = $this->methodToRulesMap[$method] ?? null;

        if ($ruleMethod) {
            $this->$ruleMethod($v);
        }

        $this->setGeneralRules($v);

        if (!$v->validate() && is_array($v->errors())) {
            throw new ValidationException($v->errors());
        }

        return $data;
    }

    public function setCreationRules(Validator $v): void
    {
    }

    public function setPatchRules(Validator $v): void
    {
    }

    public function setGeneralRules(Validator $v): void
    {
    }
}
