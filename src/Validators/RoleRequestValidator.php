<?php

declare(strict_types = 1);

namespace App\Validators;

use App\Entity\Role;
use App\Interfaces\EntityManagerServiceInterface;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use Valitron\Validator;

class RoleRequestValidator extends BaseRequestValidator
{
    /**
     * @var EntityRepository<Role>
     */
    protected EntityRepository $repository;

    public function __construct(
        protected EntityManagerServiceInterface $entityManagerService,
        protected Validator $validator
    ) {
        parent::__construct($validator);
        $objectRepository = $entityManagerService->getRepository(Role::class);
        assert($objectRepository instanceof EntityRepository);
        $this->repository = $objectRepository;
    }

    public function setCreationRules(Validator $v): void
    {
        $v->rule('required', ['name']);
    }

    public function setPatchRules(Validator $v): void
    {
    }

    /**
     * Set general validation rules for the validator object.
     *
     * @param Validator $v the validator object to set the rules on
     *
     * @return void
     */
    public function setGeneralRules(Validator $v): void
    {
        $v->rule('lengthMin', 'name', DEFAULT_NAME_MINIMAL_LENGTH);

        $v->rule(
            function ($field, $value, $params, $fields) {
                $criteria = Criteria::create()->where(Criteria::expr()->eq('name', $value));

                if (isset($fields['role'])) {
                    $criteria->andWhere(Criteria::expr()->not(Criteria::expr()->eq('id', $fields['role']->getId())));
                }

                return !$this->repository->matching($criteria)->count();
            },
            'name'
        )->message('Role with the given name already exists');
    }
}
