<?php

declare(strict_types = 1);

namespace App\Validators;

use App\Interfaces\RequestValidatorFactoryInterface;
use App\Interfaces\RequestValidatorInterface;
use Psr\Container\ContainerInterface;
use RuntimeException;

readonly class RequestValidatorFactory implements RequestValidatorFactoryInterface
{
    public function __construct(private ContainerInterface $container)
    {
    }

    public function make(string $class): RequestValidatorInterface
    {
        $validator = $this->container->get($class);

        if ($validator instanceof RequestValidatorInterface) {
            return $validator;
        }

        throw new RuntimeException('Failed to instantiate the request validator class "' . $class . '"');
    }
}
