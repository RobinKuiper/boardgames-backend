<?php

declare(strict_types = 1);

namespace App\Validators;

use App\Entity\Game;
use App\Interfaces\EntityManagerServiceInterface;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use Valitron\Validator;

class GameRequestValidator extends BaseRequestValidator
{
    /**
     * @var EntityRepository<Game>
     */
    protected readonly EntityRepository $repository;

    public function __construct(
        protected EntityManagerServiceInterface $entityManagerService,
        protected Validator $validator
    ) {
        parent::__construct($this->validator);
        $objectRepository = $entityManagerService->getRepository(Game::class);
        assert($objectRepository instanceof EntityRepository);
        $this->repository = $objectRepository;
    }

    public function setCreationRules(Validator $v): void
    {
        $v->rule('required', ['name', 'year']);
    }

    /**
     * Set general validation rules for the validator object.
     *
     * @param Validator $v the validator object to set the rules on
     *
     * @return void
     */
    public function setGeneralRules(Validator $v): void
    {
        $v->rule('lengthMin', 'name', DEFAULT_NAME_MINIMAL_LENGTH);

        $v->rule(
            function ($field, $value, $params, $fields) {
                $criteria = Criteria::create()->where(Criteria::expr()->eq('name', $value));

                if (isset($fields['game'])) {
                    $criteria->andWhere(Criteria::expr()->not(Criteria::expr()->eq('id', $fields['game']->getId())));
                }

                return !$this->repository->matching($criteria)->count();
            },
            'name'
        )->message('Game with the given name already exists');

        $v->rule(
            function ($field, $value, $params, $fields) {
                $criteria = Criteria::create()->where(Criteria::expr()->eq('bbg_id', $value));

                if (isset($fields['game'])) {
                    $criteria->andWhere(Criteria::expr()->not(Criteria::expr()->eq('id', $fields['game']->getId())));
                }

                return !$this->repository->matching($criteria)->count();
            },
            'boardGameGeekId'
        )->message('Game with the given Board Game Geek ID already exists');
    }
}
