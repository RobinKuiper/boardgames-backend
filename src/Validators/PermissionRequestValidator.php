<?php

declare(strict_types = 1);

namespace App\Validators;

use App\Entity\Permission;
use App\Interfaces\EntityManagerServiceInterface;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use Valitron\Validator;

class PermissionRequestValidator extends BaseRequestValidator
{
    /**
     * @var EntityRepository<Permission>
     */
    protected readonly EntityRepository $repository;

    public function __construct(
        protected EntityManagerServiceInterface $entityManagerService,
        protected Validator $validator
    ) {
        parent::__construct($this->validator);
        $objectRepository = $entityManagerService->getRepository(Permission::class);
        assert($objectRepository instanceof EntityRepository);
        $this->repository = $objectRepository;
    }

    public function setCreationRules(Validator $v): void
    {
        $v->rule('required', ['name', 'description']);
    }

    public function setPatchRules(Validator $v): void
    {
    }

    /**
     * Set general validation rules for the validator object.
     *
     * @param Validator $v the validator object to set the rules on
     *
     * @return void
     */
    public function setGeneralRules(Validator $v): void
    {
        $v->rule('lengthMin', 'name', DEFAULT_NAME_MINIMAL_LENGTH);
        $v->rule('lengthMin', 'description', DEFAULT_DESCRIPTION_MINIMAL_LENGTH);

        $v->rule(
            function ($field, $value, $params, $fields) {
                $criteria = Criteria::create()->where(Criteria::expr()->eq('name', $value));

                if (isset($fields['permission'])) {
                    $criteria->andWhere(Criteria::expr()->not(Criteria::expr()->eq('id', $fields['permission']->getId())));
                }

                return !$this->repository->matching($criteria)->count();
            },
            'name'
        )->message('Permission with the given name already exists');
    }
}
