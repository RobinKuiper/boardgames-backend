<?php

declare(strict_types = 1);

namespace App\Validators;

use App\Entity\Role;
use App\Entity\User;
use App\Exceptions\ValidationException;
use App\Interfaces\EntityManagerServiceInterface;
use App\Interfaces\RequestValidatorInterface;
use Doctrine\ORM\EntityRepository;
use Valitron\Validator;

class CreateUserRequestValidator implements RequestValidatorInterface
{
    /**
     * @var EntityRepository<User>
     */
    protected readonly EntityRepository $repository;

    public function __construct(
        protected EntityManagerServiceInterface $entityManagerService,
        protected Validator $validator
    ) {
        $objectRepository = $entityManagerService->getRepository(User::class);
        assert($objectRepository instanceof EntityRepository);
        $this->repository = $objectRepository;
    }

    public function validate(array $data, ?string $method = null): array
    {
        $v = new Validator($data);

        $data['role'] = array_key_exists('roleId', $data)
            ? $this->entityManagerService->getRepository(Role::class)->find($data['roleId']) : null;

        $v->rule('required', ['email', 'name', 'roleId'])->message('Required field');
        $v->rule('lengthMin', 'name', DEFAULT_NAME_MINIMAL_LENGTH)->message('Minimum 3 characters');
        $v->rule('email', 'email')->message('Invalid email');

        $v->rule(
            fn ($field, $value, $params, $fields) => !$this->repository->count(
                ['email' => $value]
            ),
            'email'
        )->message('User with the given email already exists');

        $v->rule(
            function () use ($data) {
                return !is_null($data['role']);
            },
            'roleId'
        )->message('Role with the given id does not exist');

        if (!$v->validate() && is_array($v->errors())) {
            throw new ValidationException($v->errors());
        }

        return $data;
    }
}
