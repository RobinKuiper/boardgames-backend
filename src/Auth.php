<?php

declare(strict_types = 1);

namespace App;

use App\DataObjects\JwtTokenUserData;
use App\DataObjects\RegisterUserData;
use App\Entity\Permission;
use App\Interfaces\AuthInterface;
use App\Interfaces\UserInterface;
use App\Services\UserProviderService;
use DateTime;
use DateTimeZone;
use Exception;
use Firebase\JWT\JWT;

readonly class Auth implements AuthInterface
{
    public function __construct(
        private UserProviderService $userProvider,
        private Config $config
    ) {
    }

    /**
     * @throws Exception
     */
    public function register(RegisterUserData $data): string|false
    {
        $user = $this->userProvider->createUser($data);

        return $this->generateJwtToken($user);
    }

    /**
     * Attempts to log in a user with the given credentials.
     *
     * @param array $credentials the user credentials
     *
     * @throws Exception
     *
     * @return string|false the generated JWT token if login is successful, false otherwise
     */
    public function attemptLogin(array $credentials): string|false
    {
        $user = $this->userProvider->getByCredentials($credentials);

        if (!$user || !$this->checkCredentials($user, $credentials)) {
            return false;
        }

        return $this->generateJwtToken($user);
    }

    /**
     * Checks if the given credentials are valid for the specified user.
     *
     * @param UserInterface $user the user object
     * @param array $credentials the user credentials
     *
     * @return bool returns true if the credentials are valid, false otherwise
     */
    public function checkCredentials(UserInterface $user, array $credentials): bool
    {
        return password_verify($credentials['password'], $user->getPassword());
    }

    /**
     * Generates a JWT token for the given user.
     *
     * @param UserInterface $user the user object
     *
     * @throws Exception
     *
     * @return string the generated JWT token
     */
    public function generateJwtToken(UserInterface $user): string
    {
        $expirationTimeInSeconds = $this->config->get('jwt_authentication.expiration_time');
        $expire    = (new DateTime('now', new DateTimeZone('Europe/Amsterdam')))
          ->modify("+$expirationTimeInSeconds seconds")->format('Y-m-d H:i:s');
        $secret    = $this->config->get('jwt_authentication.secret');
        $algorithm = $this->config->get('jwt_authentication.algorithm');

        $userData = new JwtTokenUserData(
            $user->getName(),
            $user->getEmail(),
            $user->getRole()->getName(),
            array_merge(
                $user->getRole()->getPermissions()->map(fn (Permission $permission) => $permission->getName())->toArray(),
                $user->getPermissions()->map(fn (Permission $permission) => $permission->getName())->toArray(),
            )
        );

        return JWT::encode([
            'expiredAt' => $expire,
            'user'      => $userData,
        ], $secret, $algorithm);
    }
}
