<?php

declare(strict_types = 1);

namespace App\Interfaces;

interface AuthInterface
{
    public function attemptLogin(array $credentials): string|false;

    public function checkCredentials(UserInterface $user, array $credentials): bool;
}
