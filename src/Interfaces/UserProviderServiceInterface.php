<?php

declare(strict_types = 1);

namespace App\Interfaces;

interface UserProviderServiceInterface
{
    public function getById(int $userId): ?UserInterface;

    public function getByCredentials(array $credentials): ?UserInterface;

    public function updatePassword(UserInterface $user, string $password): UserInterface;
}
