<?php

declare(strict_types = 1);

namespace App\Interfaces;

use Psr\Http\Message\ResponseInterface;

interface ResponseFormatterInterface
{
    public function asJson(
        ResponseInterface $response,
        mixed $data,
        int $flags = JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_QUOT | JSON_HEX_APOS | JSON_THROW_ON_ERROR
    ): ResponseInterface;
}
