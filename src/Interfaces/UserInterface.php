<?php

declare(strict_types = 1);

namespace App\Interfaces;

use App\Entity\Permission;
use App\Entity\Role;
use App\Entity\User;
use Doctrine\Common\Collections\Collection;

interface UserInterface
{
    public function getId(): int;

    public function getPassword(): string;

    public function setPassword(string $password): User;

    public function getName(): string;

    public function getEmail(): string;

    public function getRole(): Role;

    /**
     * @return Collection<int, Permission>
     */
    public function getPermissions(): Collection;

    public function addPermission(Permission $permission): void;

    public function removePermission(Permission $permission): void;
}
