<?php

declare(strict_types = 1);

namespace App\Interfaces;

interface ConfigInterface
{
    public function get(string $name, mixed $default = null): mixed;
}
