<?php

declare(strict_types = 1);

namespace App\Interfaces;

interface RequestValidatorInterface
{
    public function validate(array $data, ?string $method = null): array;
}
