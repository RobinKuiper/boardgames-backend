<?php

declare(strict_types = 1);

namespace App\Controllers;

use App\Auth;
use App\Config;
use App\DataObjects\RegisterUserData;
use App\Enum\AppEnvironment;
use App\Exceptions\ValidationException;
use App\Interfaces\RequestValidatorFactoryInterface;
use App\ResponseFormatter;
use App\Validators\RegisterUserRequestValidator;
use App\Validators\UserLoginRequestValidator;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

readonly class AuthController
{
    public function __construct(
        private RequestValidatorFactoryInterface $requestValidatorFactory,
        private Auth $auth,
        private ResponseFormatter $responseFormatter,
        private Config $config
    ) {
    }

    public function logIn(Request $request, Response $response): Response
    {
        $data = (array)$request->getParsedBody();

        $data = $this->requestValidatorFactory->make(UserLoginRequestValidator::class)->validate(
            $data
        );

        $token = $this->auth->attemptLogin($data);

        if (!$token) {
            throw new ValidationException(['password' => ['You have entered an invalid username or password']]);
        }

        $response = $this->addTokenCookie($response, $token);

        return $this->responseFormatter->asJson($response, [
            'token'   => $token,
        ]);
    }

    /**
     * @throws \Exception
     */
    public function register(Request $request, Response $response): Response
    {
        $data = $this->requestValidatorFactory->make(RegisterUserRequestValidator::class)->validate(
            $request->getParsedBody()
        );

        $token = $this->auth->register(
            new RegisterUserData($data['name'], $data['email'], $data['password'], $data['role'])
        );

        return $this->responseFormatter->asJson($response, [
            'token'   => $token,
        ]);
    }

    /**
     * @throws \Exception
     */
    public function tokenRenewal(Request $request, Response $response): Response
    {
        $token    = $this->auth->generateJwtToken($request->getAttribute("user"));
        $response = $this->addTokenCookie($response, $token);

        return $this->responseFormatter->asJson($response, [
            'token' => $token
        ]);
    }

    private function addTokenCookie(Response $response, string $token): Response
    {
        // Set the new token as an HttpOnly cookie with a specific expiration time (e.g., 7 days from now)
        $environment   = $this->config->get('environment');
        $isDevelopment = AppEnvironment::isDevelopment($environment);
        $expires       = time() + $this->config->get('jwt_authentication.cookie_expiration_time');
        $secure        = $isDevelopment ? '' : 'secure;';
        $sameSite      = $isDevelopment ? '' : 'SameSite=None;';
        $httpOnly      = $isDevelopment ? '' : 'HttpOnly;';

        return $response->withAddedHeader(
            'Set-Cookie',
            'token=' . $token . '; '.$httpOnly.$secure.$sameSite.' expires=' . gmdate('D, d M Y H:i:s T', $expires) . '; path=/'
        );
    }
}
