<?php

namespace App\Controllers;

use App\DataObjects\RegisterUserData;
use App\Entity\LogEntry;
use App\Entity\Permission;
use App\Entity\User;
use App\Interfaces\EntityManagerServiceInterface;
use App\Interfaces\RequestValidatorFactoryInterface;
use App\Mail\RetrievePasswordEmail;
use App\ResponseFormatter;
use App\Services\HashService;
use App\Services\UserProviderService;
use App\Validators\AddPermissionRequestValidator;
use App\Validators\CreateUserRequestValidator;
use Doctrine\ORM\EntityRepository;
use Gedmo\Loggable\Entity\Repository\LogEntryRepository;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

readonly class UserProviderController
{
    /**
     * @var EntityRepository<User>
     */
    protected EntityRepository $repository;

    public function __construct(
        private UserProviderService $userProviderService,
        private RequestValidatorFactoryInterface $requestValidatorFactory,
        private ResponseFormatter $responseFormatter,
        private EntityManagerServiceInterface $entityManagerService,
        protected HashService $hashService,
        protected RetrievePasswordEmail $retrievePasswordEmail
    ) {
        $objectRepository = $this->entityManagerService->getRepository(User::class);
        assert($objectRepository instanceof EntityRepository);
        $this->repository = $objectRepository;
    }

    /**
     * Retrieve all items from the repository and format the response as JSON.
     *
     * @param Response $response the response object
     *
     * @return Response the formatted response object as JSON
     */
    public function getAll(Response $response): Response
    {
        $items = $this->repository->findAll();

        return $this->responseFormatter->asJson($response, $items);
    }

    /**
     * Get the response formatted as JSON for a given user.
     *
     * @param ResponseInterface $response the original response
     * @param User $user the user object
     *
     * @return ResponseInterface the response formatted as JSON for the user
     */
    public function get(ResponseInterface $response, User $user): ResponseInterface
    {
        return $this->responseFormatter->asJson($response, $user);
    }

    public function getLogs(Response $response, User $user): ResponseInterface
    {
        $objectRepository = $this->entityManagerService->getRepository(LogEntry::class);
        assert($objectRepository instanceof LogEntryRepository);
        $logRepository = $objectRepository;

        $logs = $logRepository->getLogEntries($user);

        return $this->responseFormatter->asJson($response, $logs);
    }

    /**
     * Creates a new user and returns the provided response.
     *
     * @param Request $request the request object
     * @param ResponseInterface $response the response object
     *
     * @return ResponseInterface the provided response object
     */
    public function create(Request $request, ResponseInterface $response): ResponseInterface
    {
        $data = (array)$request->getParsedBody();

        $data = $this->requestValidatorFactory->make(CreateUserRequestValidator::class)
            ->validate($data);

        if (!array_key_exists('password', $data) || is_null($data['password'])) {
            $data['password'] = $this->hashService->generatePassword();
        }

        $user = $this->userProviderService->createUser(
            new RegisterUserData(
                $data['name'],
                $data['email'],
                $data['password'],
                $data['role']
            )
        );

        //        $this->retrievePasswordEmail->send($user, $data['password']);

        return $this->responseFormatter->asJson($response, $user);
    }

    /**
     * Deletes a user and returns the provided response.
     *
     * @param ResponseInterface $response the response object
     * @param User $user the user to be deleted
     *
     * @return ResponseInterface the provided response object
     */
    public function delete(ResponseInterface $response, User $user): ResponseInterface
    {
        $this->entityManagerService->delete($user, true);

        return $response;
    }

    /**
     * Adds a permission to a user.
     *
     * @param Request $request the request object
     * @param ResponseInterface $response the response object
     * @param User $user the user object
     *
     * @return ResponseInterface  the response object with the updated user
     */
    public function addPermission(Request $request, ResponseInterface $response, User $user): ResponseInterface
    {
        $data = (array)$request->getParsedBody();

        $data = $this->requestValidatorFactory->make(AddPermissionRequestValidator::class)->validate($data);

        $user = $this->userProviderService->addPermission($user, $data['permission']);

        return $this->responseFormatter->asJson($response, $user);
    }

    /**
     * Remove a permission from a user and sync the user in the database.
     *
     * @param Request $request the request object
     * @param ResponseInterface $response the response object
     * @param User $user the user to remove the permission from
     * @param Permission $permission the permission to remove
     *
     * @return ResponseInterface the formatted response object as JSON after removing the permission
     */
    public function removePermission(Request $request, ResponseInterface $response, User $user, Permission $permission): ResponseInterface
    {
        $user = $this->userProviderService->removePermission($user, $permission);

        return $this->responseFormatter->asJson($response, $user);
    }
}
