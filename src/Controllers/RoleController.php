<?php

declare(strict_types = 1);

namespace App\Controllers;

use App\DataObjects\RoleData;
use App\Entity\LogEntry;
use App\Entity\Permission;
use App\Entity\Role;
use App\Interfaces\EntityManagerServiceInterface;
use App\Interfaces\RequestValidatorFactoryInterface;
use App\ResponseFormatter;
use App\Services\RoleService;
use App\Validators\AddPermissionRequestValidator;
use App\Validators\RoleRequestValidator;
use Doctrine\ORM\EntityRepository;
use Gedmo\Loggable\Entity\Repository\LogEntryRepository;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

readonly class RoleController
{
    /**
     * @var EntityRepository<Role>
     */
    protected EntityRepository $repository;

    public function __construct(
        private RoleService $roleService,
        private RequestValidatorFactoryInterface $requestValidatorFactory,
        private ResponseFormatter $responseFormatter,
        private EntityManagerServiceInterface $entityManagerService
    ) {
        $objectRepository = $this->entityManagerService->getRepository(Role::class);
        assert($objectRepository instanceof EntityRepository);
        $this->repository = $objectRepository;
    }

    /**
     * Retrieve all items from the repository and format the response as JSON.
     *
     * @param Response $response the response object
     *
     * @return Response the formatted response object as JSON
     */
    public function getAll(Response $response): Response
    {
        $items = $this->repository->findAll();

        return $this->responseFormatter->asJson($response, $items);
    }

    /**
     * Get role.
     *
     * Retrieves the response data from the API for the given response and role.
     *
     * @param ResponseInterface $response the response object
     * @param Role   $role   the role object
     *
     * @return ResponseInterface the formatted response in JSON format
     */
    public function get(ResponseInterface $response, Role $role): ResponseInterface
    {
        return $this->responseFormatter->asJson($response, $role);
    }

    public function getLogs(Response $response, Role $role): ResponseInterface
    {
        $objectRepository = $this->entityManagerService->getRepository(LogEntry::class);
        assert($objectRepository instanceof LogEntryRepository);
        $logRepository = $objectRepository;

        $logs = $logRepository->getLogEntries($role);

        return $this->responseFormatter->asJson($response, $logs);
    }

    /**
     * Create role.
     *
     * Creates a new role using the provided request data, saves it in the
     * database and returns a formatted JSON response.
     *
     * @param Request  $request  the request object
     * @param ResponseInterface $response the response object
     *
     * @return ResponseInterface the formatted response in JSON format
     */
    public function create(Request $request, ResponseInterface $response): ResponseInterface
    {
        $data = $this->validateRequestData($request);

        $role = $this->roleService->create(new RoleData($data['name']));

        return $this->responseFormatter->asJson($response, $role);
    }

    /**
     * Update role.
     *
     * Updates the role data with the given request, response, and role.
     *
     * @param Request  $request  the request object
     * @param ResponseInterface $response the response object
     * @param Role   $role   the role object
     *
     * @return ResponseInterface the formatted response in JSON format
     */
    public function update(Request $request, ResponseInterface $response, Role $role): ResponseInterface
    {
        $data = $this->validateRequestData($request, $role);

        $role = $this->roleService->update($role, new RoleData($data['name']));

        return $this->responseFormatter->asJson($response, $role);
    }

    /**
     * Update a role using the provided request data and format the response as JSON.
     *
     * @param Request $request the request object containing the update data
     * @param Response $response the response object
     * @param Role $role the role object to update
     *
     * @return ResponseInterface the formatted response object as JSON
     */
    public function patch(Request $request, Response $response, Role $role): ResponseInterface
    {
        $data = $this->validateRequestData($request, $role);

        $role = $this->roleService->patch($role, $data);

        return $this->responseFormatter->asJson($response, $role);
    }

    /**
     * Delete role.
     *
     * Deletes the role from the database using the EntityManagerService.
     *
     * @param ResponseInterface $response the response object
     * @param Role   $role   the role object to be deleted
     *
     * @return ResponseInterface the original response object
     */
    public function delete(ResponseInterface $response, Role $role): ResponseInterface
    {
        $this->entityManagerService->delete($role, true);

        return $response;
    }

    /**
     * Add permission.
     *
     * Adds a permission to the given role.
     *
     * @param Request $request the request object
     * @param ResponseInterface $response the response object
     * @param Role $role the role object
     *
     * @return ResponseInterface the formatted response in JSON format
     */
    public function addPermission(Request $request, ResponseInterface $response, Role $role): ResponseInterface
    {
        $data = (array)$request->getParsedBody();

        $data = $this->requestValidatorFactory->make(AddPermissionRequestValidator::class)->validate($data, $request->getMethod());

        $role = $this->roleService->addPermission($role, $data['permission']);

        return $this->responseFormatter->asJson($response, $role);
    }

    /**
     * Remove a permission from a role and synchronize the changes with the database.
     *
     * @param Request $request the request object
     * @param ResponseInterface $response the response object
     * @param Role $role the role from which the permission should be removed
     * @param Permission $permission the permission to be removed from the role
     *
     * @return ResponseInterface the response object after removing the permission and synchronizing the changes
     */
    public function removePermission(Request $request, ResponseInterface $response, Role $role, Permission $permission): ResponseInterface
    {
        $role = $this->roleService->removePermission($role, $permission);

        return $this->responseFormatter->asJson($response, $role);
    }

    /**
     * Validate the request data and return the validated data as an array.
     *
     * @param Request $request the request object
     * @param Role|null $role (optional) the client object
     *
     * @return array the validated request data
     */
    private function validateRequestData(Request $request, ?Role $role = null): array
    {
        $data = (array)$request->getParsedBody();

        if ($role) {
            $data['role'] = $role;
        }

        return $this->requestValidatorFactory->make(RoleRequestValidator::class)
          ->validate($data, $request->getMethod());
    }
}
