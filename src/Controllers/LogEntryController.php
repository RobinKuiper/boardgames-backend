<?php

declare(strict_types = 1);

namespace App\Controllers;

use App\Entity\LogEntry;
use App\Interfaces\EntityManagerServiceInterface;
use App\ResponseFormatter;
use Doctrine\ORM\EntityRepository;
use Psr\Http\Message\ResponseInterface as Response;

readonly class LogEntryController
{
    /**
     * @var EntityRepository<LogEntry>
     */
    protected EntityRepository $repository;

    public function __construct(
        private ResponseFormatter $responseFormatter,
        private EntityManagerServiceInterface $entityManagerService
    ) {
        $objectRepository = $this->entityManagerService->getRepository(LogEntry::class);
        assert($objectRepository instanceof EntityRepository);
        $this->repository = $objectRepository;
    }

    /**
     * Retrieve all items from the repository and format the response as JSON.
     *
     * @param Response $response the response object
     *
     * @return Response the formatted response object as JSON
     */
    public function getAll(Response $response): Response
    {
        $items = $this->repository->findAll();

        return $this->responseFormatter->asJson($response, $items);
    }

    /**
     * Get logEntry.
     *
     * Returns a response after formatting it as JSON using the provided response and logEntry.
     *
     * @param Response    $response    the response object to format as JSON
     * @param LogEntry $logEntry the logEntry object to use while formatting the response
     *
     * @return Response the formatted response object
     */
    public function get(Response $response, LogEntry $logEntry): Response
    {
        return $this->responseFormatter->asJson($response, $logEntry);
    }
}
