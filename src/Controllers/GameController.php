<?php

declare(strict_types = 1);

namespace App\Controllers;

use App\DataObjects\GameData;
use App\Entity\Game;
use App\Entity\LogEntry;
use App\Entity\User;
use App\Interfaces\EntityManagerServiceInterface;
use App\Interfaces\RequestValidatorFactoryInterface;
use App\ResponseFormatter;
use App\Services\GameService;
use App\Services\RequestService;
use App\Validators\GameRequestValidator;
use Doctrine\ORM\EntityRepository;
use Exception;
use Gedmo\Loggable\Entity\Repository\LogEntryRepository;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

readonly class GameController
{
    /**
     * @var EntityRepository<Game>
     */
    protected EntityRepository $repository;

    /**
     * @var EntityRepository<User>
     */
    protected EntityRepository $userRepository;

    public function __construct(
        private GameService                      $gameService,
        private RequestValidatorFactoryInterface $requestValidatorFactory,
        private ResponseFormatter                $responseFormatter,
        private EntityManagerServiceInterface    $entityManagerService,
        private RequestService                   $requestService
    ) {
        $objectRepository = $this->entityManagerService->getRepository(Game::class);
        assert($objectRepository instanceof EntityRepository);
        $this->repository = $objectRepository;

        $objectRepository = $this->entityManagerService->getRepository(User::class);
        assert($objectRepository instanceof EntityRepository);
        $this->userRepository = $objectRepository;
    }

    /**
     * Retrieve all items from the repository and format the response as JSON.
     *
     * @param Request $request
     * @param ResponseInterface $response the response object
     *
     * @return ResponseInterface the formatted response object as JSON
     * @throws Exception
     */
    public function getAll(Request $request, Response $response): Response
    {
        $params      = $this->requestService->getDataTableQueryParameters($request);
        $games       = $this->gameService->getPaginatedResults($params);
        $transformer = function (Game $game) {
            return $game;
        };

        $total = count($games);

        return $this->responseFormatter->asDataTable(
            $response,
            array_map($transformer, (array) $games->getIterator()),
            $params->draw,
            $total
        );
    }

    public function getMine(Request $request, Response $response): Response
    {
        $user = $request->getAttribute('user');

        return $this->responseFormatter->asJson($response, $user->getGames()->toArray());
    }

    public function getOurs(Request $request, Response $response): Response
    {
        $users = $this->userRepository->findAll();

        $games = [];
        /** @var User $user */
        foreach ($users as $user) {
            /** @var Game $game */
            foreach ($user->getGames() as $game) {
                $games[] = [
                    'id' => $game->getId(),
                    'name' => $game->getName(),
                    'year' => $game->getYear(),
                    'boardGameGeekId' => $game->getBoardGameGeekId(),
                    'boardGameGeekRank' => $game->getBoardGameGeekRank(),
                    'boardGameGeekAverage' => $game->getBoardGameGeekAverage(),
                    'user' => $user->getName()
                ];
            }
        }

        return $this->responseFormatter->asJson($response, $games);
    }

    /**
     * Get game.
     *
     * Retrieves the response data from the API for the given response and game.
     *
     * @param ResponseInterface $response the response object
     * @param Game   $game   the game object
     *
     * @return ResponseInterface the formatted response in JSON format
     */
    public function get(ResponseInterface $response, Game $game): ResponseInterface
    {
        return $this->responseFormatter->asJson($response, $game);
    }

    public function getLogs(ResponseInterface $response, Game $game): ResponseInterface
    {
        $objectRepository = $this->entityManagerService->getRepository(LogEntry::class);
        assert($objectRepository instanceof LogEntryRepository);
        $logRepository = $objectRepository;

        $logs = $logRepository->getLogEntries($game);

        return $this->responseFormatter->asJson($response, $logs);
    }

    /**
     * Create game.
     *
     * Creates a new game using the provided request data, saves it in the
     * database and returns a formatted JSON response.
     *
     * @param Request  $request  the request object
     * @param ResponseInterface $response the response object
     *
     * @return ResponseInterface the formatted response in JSON format
     */
    public function create(Request $request, ResponseInterface $response): ResponseInterface
    {
        $data = $this->validateRequestData($request);

        $game = $this->gameService->create(new GameData(
            $data['name'],
            $data['pricePerPerson'] ?? null,
            $data['date'] ?? null
        ));

        return $this->responseFormatter->asJson($response, $game);
    }

    public function addUser(Request $request, ResponseInterface $response, Game $game): ResponseInterface
    {
        $user = $request->getAttribute('user');
        $game = $this->gameService->addUser($game, $user);

        return $this->responseFormatter->asJson($response, $game);
    }

    public function removeUser(Request $request, ResponseInterface $response, Game $game, User $user): ResponseInterface
    {
        $game = $this->gameService->removeUser($game, $user);

        return $this->responseFormatter->asJson($response, $game);
    }

    /**
     * Update game.
     *
     * Updates the game data with the given request, response, and game.
     *
     * @param Request  $request  the request object
     * @param ResponseInterface $response the response object
     * @param Game   $game   the game object
     *
     * @return ResponseInterface the formatted response in JSON format
     */
    public function update(Request $request, ResponseInterface $response, Game $game): ResponseInterface
    {
        $data = $this->validateRequestData($request, $game);

        $game = $this->gameService->update($game, new GameData(
            $data['name'],
            $data['pricePerPerson'] ?? null,
            $data['date'] ?? null
        ));

        return $this->responseFormatter->asJson($response, $game);
    }

    /**
     * Update a game using PATCH method.
     *
     * @param Request $request the request object
     * @param ResponseInterface $response the response object
     * @param Game $game the game to update
     *
     * @return ResponseInterface the updated game as JSON response
     */
    public function patch(Request $request, ResponseInterface $response, Game $game): ResponseInterface
    {
        $data = $this->validateRequestData($request, $game);

        $game = $this->gameService->patch($game, $data);

        return $this->responseFormatter->asJson($response, $game);
    }

    /**
     * Delete game.
     *
     * Deletes the game from the database using the EntityManagerService.
     *
     * @param ResponseInterface $response the response object
     * @param Game   $game   the game object to be deleted
     *
     * @return ResponseInterface the original response object
     */
    public function delete(ResponseInterface $response, Game $game): ResponseInterface
    {
        $this->entityManagerService->delete($game, true);

        return $response;
    }

    /**
     * Validate the request data and return the validated data as an array.
     *
     * @param Request $request the request object
     * @param Game|null $game (optional) the game object
     *
     * @return array the validated request data
     */
    private function validateRequestData(Request $request, ?Game $game = null): array
    {
        $data = (array)$request->getParsedBody();

        if ($game) {
            $data['game'] = $game;
        }

        return $this->requestValidatorFactory->make(GameRequestValidator::class)
          ->validate($data, $request->getMethod());
    }
}
