<?php

declare(strict_types = 1);

namespace App\Controllers;

use App\DataObjects\PermissionData;
use App\Entity\LogEntry;
use App\Entity\Permission;
use App\Interfaces\EntityManagerServiceInterface;
use App\Interfaces\RequestValidatorFactoryInterface;
use App\ResponseFormatter;
use App\Services\PermissionService;
use App\Validators\PermissionRequestValidator;
use Doctrine\ORM\EntityRepository;
use Gedmo\Loggable\Entity\Repository\LogEntryRepository;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface as Request;

readonly class PermissionController
{
    /**
     * @var EntityRepository<Permission>
     */
    protected EntityRepository $repository;

    public function __construct(
        private PermissionService $permissionService,
        private RequestValidatorFactoryInterface $requestValidatorFactory,
        private ResponseFormatter $responseFormatter,
        private EntityManagerServiceInterface $entityManagerService
    ) {
        $objectRepository = $this->entityManagerService->getRepository(Permission::class);
        assert($objectRepository instanceof EntityRepository);
        $this->repository = $objectRepository;
    }

    /**
     * Retrieve all items from the repository and format the response as JSON.
     *
     * @param ResponseInterface $response the response object
     *
     * @return ResponseInterface the formatted response object as JSON
     */
    public function getAll(ResponseInterface $response): ResponseInterface
    {
        $items = $this->repository->findAll();

        return $this->responseFormatter->asJson($response, $items);
    }

    /**
     * Get permission.
     *
     * Retrieves the response data from the API for the given response and permission.
     *
     * @param ResponseInterface $response the response object
     * @param Permission   $permission   the permission object
     *
     * @return ResponseInterface the formatted response in JSON format
     */
    public function get(ResponseInterface $response, Permission $permission): ResponseInterface
    {
        return $this->responseFormatter->asJson($response, $permission);
    }

    public function getLogs(ResponseInterface $response, Permission $permission): ResponseInterface
    {
        $objectRepository = $this->entityManagerService->getRepository(LogEntry::class);
        assert($objectRepository instanceof LogEntryRepository);
        $logRepository = $objectRepository;

        $logs = $logRepository->getLogEntries($permission);

        return $this->responseFormatter->asJson($response, $logs);
    }

    /**
     * Create permission.
     *
     * Creates a new permission using the provided request data, saves it in the
     * database and returns a formatted JSON response.
     *
     * @param Request  $request  the request object
     * @param ResponseInterface $response the response object
     *
     * @return ResponseInterface the formatted response in JSON format
     */
    public function create(Request $request, ResponseInterface $response): ResponseInterface
    {
        $data = $this->validateRequestData($request);

        $permission = $this->permissionService->create($this->getPermissionData($data));

        return $this->responseFormatter->asJson($response, $permission);
    }

    /**
     * Update permission.
     *
     * Updates the permission data with the given request, response, and permission.
     *
     * @param Request  $request  the request object
     * @param ResponseInterface $response the response object
     * @param Permission   $permission   the permission object
     *
     * @return ResponseInterface the formatted response in JSON format
     */
    public function update(Request $request, ResponseInterface $response, Permission $permission): ResponseInterface
    {
        $data = $this->validateRequestData($request, $permission);

        $permission = $this->permissionService->update($permission, $this->getPermissionData($data));

        return $this->responseFormatter->asJson($response, $permission);
    }

    /**
     * Process a PATCH request by updating a permission.
     *
     * @param Request $request the HTTP request object
     * @param ResponseInterface $response the HTTP response object
     * @param Permission $permission the permission to update
     *
     * @return ResponseInterface the updated HTTP response object
     */
    public function patch(Request $request, ResponseInterface $response, Permission $permission): ResponseInterface
    {
        $data = $this->validateRequestData($request, $permission);

        $permission = $this->permissionService->patch($permission, $data);

        return $this->responseFormatter->asJson($response, $permission);
    }

    /**
     * Delete permission.
     *
     * Deletes the permission from the database using the EntityManagerService.
     *
     * @param ResponseInterface $response the response object
     * @param Permission   $permission   the permission object to be deleted
     *
     * @return ResponseInterface the original response object
     */
    public function delete(ResponseInterface $response, Permission $permission): ResponseInterface
    {
        $this->entityManagerService->delete($permission, true);

        return $response;
    }

    /**
     * Validate request data.
     *
     * @param Request $request the Request object
     * @param Permission|null $permission the Permission object (optional)
     *
     * @return array the validated request data
     */
    private function validateRequestData(Request $request, ?Permission $permission = null): array
    {
        $data = (array)$request->getParsedBody();

        if ($permission) {
            $data['permission'] = $permission;
        }

        return $this->requestValidatorFactory->make(PermissionRequestValidator::class)
          ->validate($data, $request->getMethod());
    }

    /**
     * Retrieve permission data from given array.
     *
     * @param array $data the array containing permission data
     *
     * @return PermissionData the PermissionData object created from the given array
     */
    private function getPermissionData(array $data): PermissionData
    {
        return new PermissionData($data['name'], $data['description']);
    }
}
