<?php

declare(strict_types = 1);

namespace App;

use App\Interfaces\EntityManagerServiceInterface;
use Closure;
use InvalidArgumentException;
use LogicException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use ReflectionException;
use ReflectionFunction;
use ReflectionFunctionAbstract;
use ReflectionMethod;
use ReflectionNamedType;
use ReflectionParameter;
use Slim\Exception\HttpNotFoundException;
use Slim\Interfaces\InvocationStrategyInterface;

readonly class RouteEntityBindingStrategy implements InvocationStrategyInterface
{
    public function __construct(
        private EntityManagerServiceInterface $entityManagerService,
    ) {
    }

    /**
     * Invokes the given callable with resolved arguments.
     *
     * @param callable $callable the callable to invoke
     * @param ServerRequestInterface $request the server request object
     * @param ResponseInterface $response the response object
     * @param array $routeArguments the route arguments
     *
     * @throws ReflectionException
     *
     * @return ResponseInterface the response returned by the callable
     */
    public function __invoke(
        callable $callable,
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $routeArguments
    ): ResponseInterface {
        $callableReflection = $this->createReflectionForCallable($callable);
        $resolvedArguments  = $this->resolveArguments($callableReflection, $request, $response, $routeArguments);

        return $callable(...$resolvedArguments);
    }

    /**
     * Resolves the values of method parameters.
     *
     * @param ReflectionFunctionAbstract $callableReflection the reflection object of the callable
     * @param ServerRequestInterface     $request            the server request object
     * @param ResponseInterface          $response           the response object
     * @param array                      $routeArguments     the route arguments
     *
     * @return array an array of resolved values for the parameters
     */
    private function resolveArguments(
        ReflectionFunctionAbstract $callableReflection,
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $routeArguments
    ): array {
        $resolvedArguments = [];

        foreach ($callableReflection->getParameters() as $parameter) {
            if ($resolvedArgument = $this->resolveArgument($parameter, $request, $response, $routeArguments)) {
                $resolvedArguments[] = $resolvedArgument;
            }
        }

        return $resolvedArguments;
    }

    /**
     * Resolves the value of a method parameter.
     *
     * @param ReflectionParameter   $parameter      the parameter to resolve
     * @param ServerRequestInterface $request        the server request object
     * @param ResponseInterface      $response       the response object
     * @param array                  $routeArguments the route arguments
     *
     * @return mixed|null the resolved value of the parameter or null if it cannot be resolved
     */
    private function resolveArgument(
        ReflectionParameter $parameter,
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $routeArguments
    ): mixed {
        $type = $parameter->getType();

        if (!$type) {
            return null;
        }

        if (!$type instanceof ReflectionNamedType) {
            throw new LogicException('Unsupported type found.');
        }

        $paramName = $parameter->getName();
        $typeName  = $type->getName();

        if ($type->isBuiltin()) {
            return $this->resolveBuiltinType($paramName, $typeName, $routeArguments);
        }

        return $this->resolveObjectType($parameter, $typeName, $request, $response, $routeArguments);
    }

    /**
     * Resolves the value of a built-in type method parameter.
     *
     * @param string $paramName      the name of the parameter
     * @param string $typeName       the name of the type
     * @param array  $routeArguments the route arguments
     *
     * @return array|null the resolved value of the parameter or null if it cannot be resolved
     */
    private function resolveBuiltinType(string $paramName, string $typeName, array $routeArguments): ?array
    {
        if ($typeName === 'array' && $paramName === 'args') {
            return $routeArguments;
        }

        return null;
    }

    /**
     * Resolves the type of object based on the given parameters.
     *
     * @param ReflectionParameter   $parameter      the reflection parameter
     * @param string                 $typeName       the name of the type
     * @param ServerRequestInterface $request        the server request
     * @param ResponseInterface      $response       the response
     * @param array                  $routeArguments the route arguments
     *
     * @throws InvalidArgumentException if the argument cannot be resolved or allows null
     * @throws HttpNotFoundException if the entity with a specific id is not found
     *
     * @return mixed the resolved object
     */
    private function resolveObjectType(
        ReflectionParameter $parameter,
        string $typeName,
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $routeArguments
    ): mixed {
        $paramName = $parameter->getName();

        if ($typeName === ServerRequestInterface::class) {
            return $request;
        }

        if ($typeName === ResponseInterface::class) {
            return $response;
        }

        $entityId = $routeArguments[$paramName] ?? null;

        if (!$entityId || $parameter->allowsNull()) {
            throw new InvalidArgumentException(
                'Unable to resolve argument "' . $paramName . '" in the callable'
            );
        }

        $entity = $this->entityManagerService->find($typeName, $entityId);

        if (!$entity) {
            throw new HttpNotFoundException($request, "$paramName with id $entityId not found");
        }

        return $entity;
    }

    /**
     * Create a reflection object for the given callable.
     *
     * @param callable $callable the callable to create a reflection for
     *
     * @throws ReflectionException
     *
     * @return ReflectionFunctionAbstract the reflection object for the callable
     */
    public function createReflectionForCallable(callable $callable): ReflectionFunctionAbstract
    {
        if (is_array($callable)) {
            // Handles: [$object, 'methodName'] and ['ClassName', 'staticMethodName']
            return new ReflectionMethod($callable[0], $callable[1]);
        }

        if (is_string($callable) || $callable instanceof Closure) {
            // Handles: 'functionName'
            return new ReflectionFunction($callable);
        }

        throw new InvalidArgumentException('Unrecognized type of callable');
    }
}
