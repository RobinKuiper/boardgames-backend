<?php

declare(strict_types = 1);

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Throwable;

class Deploy extends Command
{
    protected static $defaultName        = 'app:deploy';
    protected static $defaultDescription = 'Deploy the app';

    protected string $cacheDir           = CACHE_PATH;

    /**
     * Execute the deployment command.
     *
     * @param InputInterface $input the input interface
     * @param OutputInterface $output the output interface to write messages
     *
     * @throws Throwable
     *
     * @return int the exit status code
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->runAsciiArt($output);
        $appName = $_ENV['APP_NAME'];
        $output->writeln("<info>Deploying $appName</info>");

        $this->runMigrations($output);
        $this->deleteCache($output);

        $output->writeln('<info>Deployment completed, enjoy!</info>');

        return Command::SUCCESS;
    }

    /**
     * Runs the database migrations.
     *
     * @param OutputInterface $output the output interface to write messages
     *
     * @throws Throwable
     *
     * @return void
     */
    private function runMigrations(OutputInterface $output): void
    {
        $output->writeln('<comment>Running migrations</comment>');

        $migrateInput = new ArrayInput([
            'command' => 'migrations:migrate',
            '--no-interaction' => true,
        ]);

        $this->getApplication()?->doRun($migrateInput, $output);

        $output->writeln('<info>Migrating completed successfully</info>');
    }

    /**
     * Deletes the router cache file.
     *
     * @param OutputInterface $output the output interface to write messages
     *
     * @return void
     */
    private function deleteCache(OutputInterface $output): void
    {
        $output->writeln('<comment>Removing cache</comment>');

        if (!is_dir($this->cacheDir)) {
            $output->writeln('<comment>Cache directory doesn\'t exist</comment>');
        }

        $glob = glob($this->cacheDir . '/*');

        if (is_array($glob)) {
            foreach ($glob as $file) {
                if (is_file($file)) {
                    if (!unlink($file)) {
                        $output->writeln("<error>Couldn't remove $file</error>");
                    }
                }
            }
        }

        $output->writeln('<comment>Cache removed successfully</comment>');
    }

    /**
     * Runs the runAsciiArt method to display an ASCII art in the output.
     *
     * @param OutputInterface $output the output instance
     *
     * @return void
     */
    private function runAsciiArt(OutputInterface $output): void
    {
        $lines = [
            '@@@@@@@@@@@@@////////////#@@@@@@@@@@@@@@@@@@@@@@@@@@@/////////////#@@@@@@@@@@@@@',
            '@@@@@@@&//////////////////////(@@@@@@@@@@@@@@@@////////////////////////(@@@@@@@@',
            '@@@@@(///////////////////////////&@@@@@@@@@@%//////////////////////////////@@@@@',
            '@@&//////////////&&&&&(////////////@@@@@@@(//////////////&&&&&(//////////////@@@',
            '@@///////////&@@@@@@@@@@@%//////////@@@@@///////////&@@@@@@@@@@@@@%///////////%@',
            '@//////////@@@@@@@@@@@@@@@@#////////(@@%//////////#@@@@@@@@@@@@@@@@@#//////////%',
            '//////////&@@@@@@@@@@@@@@@@@@@@@@@@@@@@#/////////#@@@@@@@@@@@@@@@@@@@&//////////',
            '/////////%@@@@@@@@@@@@@@@@@@@@@@@@@@@@%/////////(@@@@@@@@@@@@@@@@@@@@@//////////',
            '/////////%@@@@@@@@@&/////////////////##/////////(@@@@@@@@@@@@@@@@@@@@@@/////////',
            '/////////%@@@@@@@@@&/////////////////#%/////////(@@@@@@@@@@@@@@@@@@@@@//////////',
            '/////////#@@@@@@@@@&/////////////////#@(/////////#@@@@@@@@@@@@@@@@@@@@//////////',
            '#/////////%@@@@@@@@@@@@@@@@@(////////#@%//////////%@@@@@@@@@@@@@@@@@&//////////%',
            '@(//////////&@@@@@@@@@@@@@@(/////////#@@#///////////&@@@@@@@@@@@@@@#//////////#@',
            '@@&////////////%&&&@&&&&/////////////#@@@@/////////////#&&&@&&&&/////////////#@@',
            '@@@@#////////////////////////////////#@@@@@&///////////////////////////////(@@@@',
            '@@@@@@@///////////////////////@//////#@@@@@@@@(/////////////////////////#@@#((((',
            '@@@@@@@@@@@(//////////////%@@@@//////#@@@@@@@@@@@@@(////////////////&@@@@@@#(((#',
        ];

        foreach ($lines as $line) {
            $output->writeln('<comment>' . $line . '</comment>');
        }
    }
}
