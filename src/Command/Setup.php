<?php

declare(strict_types = 1);

namespace App\Command;

use Exception;
use Phinx\Console\PhinxApplication;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\ExceptionInterface;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;
use Throwable;

class Setup extends Command
{
    protected static $defaultName        = 'app:setup';
    protected static $defaultDescription = 'Setup the app';
    private string $envFilePath = __DIR__ . '/../../.env';

    public function __construct(private readonly QuestionHelper $questionHelper)
    {
        parent::__construct();
    }

    /**
     * Executes the method to generate a new JWT_SECRET and save it in the .env file.
     *
     * @param InputInterface $input the input interface
     * @param OutputInterface $output the output interface
     *
     * @throws Throwable
     * @throws Exception
     *
     * @return int the command status
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->runAsciiArt($output);
        $appName = $_ENV['APP_NAME'];
        $output->writeln("<info>Setup $appName</info>");

        $isProduction = $this->setEnvironment($input, $output);
        $this->setJwtKey($output);
        $this->runMigrations($output);
        $this->createAdminUser($input, $output);

        if (!$isProduction) {
            $question = new ConfirmationQuestion(
                '<question>Do you want to run the database seeder? (y/N)</question>',
                false
            );

            if ($this->questionHelper->ask($input, $output, $question)) {
                $this->runSeeder($output);
            }
        }

        $output->writeln('<info>Installation completed, success!</info>');

        return Command::SUCCESS;
    }

    /**
     * Sets the environment based on the user's input.
     *
     * @param InputInterface $input the input interface to get user input
     * @param OutputInterface $output the output interface to display messages
     *
     * @return bool returns true if the environment is set to production, false otherwise
     */
    private function setEnvironment(InputInterface $input, OutputInterface $output): bool
    {
        $question = new ConfirmationQuestion(
            '<question>Do you want to set up on a production environment? (y/N)</question>',
            false
        );

        $isProduction = $this->questionHelper->ask($input, $output, $question);
        $environment  = $isProduction ? 'production' : 'development';

        $envFileContent = (string)file_get_contents($this->envFilePath);

        $pattern = '/^APP_ENV=.*/m';

        if (preg_match($pattern, $envFileContent)) {
            $envFileContent = preg_replace($pattern, 'APP_ENV=' . $environment, $envFileContent);
        } else {
            $envFileContent .= PHP_EOL . 'APP_ENV=' . $environment;
        }

        file_put_contents($this->envFilePath, $envFileContent);

        $output->writeln("<info>Environment $environment has been set.</info>");

        return $isProduction;
    }

    /**
     * Sets the JWT key by generating a new secret using the `app:generate:jwt-secret` command.
     *
     * @param OutputInterface $output the output interface to display the command's output
     *
     * @throws Throwable
     *
     * @return void
     */
    private function setJwtKey(OutputInterface $output): void
    {
        $jwtKeyInput = new ArrayInput([
            'command' => 'app:generate:jwt-secret',
        ]);

        $this->getApplication()?->doRun($jwtKeyInput, $output);
    }

    /**
     * Runs database migrations using the `migrations:migrate` command.
     *
     * @param OutputInterface $output the output interface to display the command's output
     *
     * @throws Throwable in case of any error during the migration process
     *
     * @return void
     */
    private function runMigrations(OutputInterface $output): void
    {
        $migrateInput = new ArrayInput([
            'command' => 'migrations:migrate',
        ]);

        $this->getApplication()?->doRun($migrateInput, $output);
    }

    /**
     * Creates an admin user by running the `app:add:admin` command.
     *
     * @param InputInterface $input
     * @param OutputInterface $output the output interface to display the command's output
     *
     * @throws Throwable
     *
     * @return void
     */
    private function createAdminUser(InputInterface $input, OutputInterface $output): void
    {
        $createAdminInput = new ArrayInput([
            'command' => 'app:add:admin',
        ]);

        $this->getApplication()?->doRun($createAdminInput, $output);

        $question = new Question(
            '<comment>Please write down your password and press enter to continue...</comment>'
        );

        $this->questionHelper->ask($input, $output, $question);
    }

    /**
     * Runs the seeder command to seed the database using Phinx.
     *
     * @param OutputInterface $output the output instance
     *
     * @throws ExceptionInterface
     *
     * @return int returns the exit status code of the seeder command
     */
    private function runSeeder(OutputInterface $output): int
    {
        $arguments = [
            'command' => 'migrate',
            '-c' => __DIR__ . '/../../phinx.php',
            '-e' => 'development',
            '-vvv' => true,
        ];

        return (new PhinxApplication())->find('seed:run')->run(new ArrayInput($arguments), $output);
    }

    /**
     * Runs the runAsciiArt method to display an ASCII art in the output.
     *
     * @param OutputInterface $output the output instance
     *
     * @return void
     */
    private function runAsciiArt(OutputInterface $output): void
    {
        $lines = [
            '⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣀⡠⢤⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀',
            '⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⡴⠟⠃⠀⠀⠙⣄⠀⠀⠀⠀⠀⠀⠀⠀⠀',
            '⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣠⠋⠀⠀⠀⠀⠀⠀⠘⣆⠀⠀⠀⠀⠀⠀⠀⠀',
            '⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢠⠾⢛⠒⠀⠀⠀⠀⠀⠀⠀⢸⡆⠀⠀⠀⠀⠀⠀⠀',
            '⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣿⣶⣄⡈⠓⢄⠠⡀⠀⠀⠀⣄⣷⠀⠀⠀⠀⠀⠀⠀',
            '⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣿⣷⠀⠈⠱⡄⠑⣌⠆⠀⠀⡜⢻⠀⠀⠀⠀⠀⠀⠀',
            '⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢸⣿⡿⠳⡆⠐⢿⣆⠈⢿⠀⠀⡇⠘⡆⠀⠀⠀⠀⠀⠀',
            '⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢿⣿⣷⡇⠀⠀⠈⢆⠈⠆⢸⠀⠀⢣⠀⠀⠀⠀⠀⠀',
            '⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠘⣿⣿⣿⣧⠀⠀⠈⢂⠀⡇⠀⠀⢨⠓⣄⠀⠀⠀⠀',
            '⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣸⣿⣿⣿⣦⣤⠖⡏⡸⠀⣀⡴⠋⠀⠈⠢⡀⠀⠀',
            '⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢠⣾⠁⣹⣿⣿⣿⣷⣾⠽⠖⠊⢹⣀⠄⠀⠀⠀⠈⢣⡀',
            '⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⡟⣇⣰⢫⢻⢉⠉⠀⣿⡆⠀⠀⡸⡏⠀⠀⠀⠀⠀⠀⢇',
            '⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢨⡇⡇⠈⢸⢸⢸⠀⠀⡇⡇⠀⠀⠁⠻⡄⡠⠂⠀⠀⠀⠘',
            '⢤⣄⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢠⠛⠓⡇⠀⠸⡆⢸⠀⢠⣿⠀⠀⠀⠀⣰⣿⣵⡆⠀⠀⠀⠀',
            '⠈⢻⣷⣦⣀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣠⡿⣦⣀⡇⠀⢧⡇⠀⠀⢺⡟⠀⠀⠀⢰⠉⣰⠟⠊⣠⠂⠀⡸',
            '⠀⠀⢻⣿⣿⣷⣦⣀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣠⢧⡙⠺⠿⡇⠀⠘⠇⠀⠀⢸⣧⠀⠀⢠⠃⣾⣌⠉⠩⠭⠍⣉⡇',
            '⠀⠀⠀⠻⣿⣿⣿⣿⣿⣦⣀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣠⣞⣋⠀⠈⠀⡳⣧⠀⠀⠀⠀⠀⢸⡏⠀⠀⡞⢰⠉⠉⠉⠉⠉⠓⢻⠃',
            '⠀⠀⠀⠀⠹⣿⣿⣿⣿⣿⣿⣷⡄⠀⠀⢀⣀⠠⠤⣤⣤⠤⠞⠓⢠⠈⡆⠀⢣⣸⣾⠆⠀⠀⠀⠀⠀⢀⣀⡼⠁⡿⠈⣉⣉⣒⡒⠢⡼⠀',
            '⠀⠀⠀⠀⠀⠘⣿⣿⣿⣿⣿⣿⣿⣎⣽⣶⣤⡶⢋⣤⠃⣠⡦⢀⡼⢦⣾⡤⠚⣟⣁⣀⣀⣀⣀⠀⣀⣈⣀⣠⣾⣅⠀⠑⠂⠤⠌⣩⡇⠀',
            '⠀⠀⠀⠀⠀⠀⠘⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡁⣺⢁⣞⣉⡴⠟⡀⠀⠀⠀⠁⠸⡅⠀⠈⢷⠈⠏⠙⠀⢹⡛⠀⢉⠀⠀⠀⣀⣀⣼⡇⠀',
            '⠀⠀⠀⠀⠀⠀⠀⠀⠈⠻⣿⣿⣿⣿⣿⣿⣿⣿⣽⣿⡟⢡⠖⣡⡴⠂⣀⣀⣀⣰⣁⣀⣀⣸⠀⠀⠀⠀⠈⠁⠀⠀⠈⠀⣠⠜⠋⣠⠁⠀',
            '⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠙⢿⣿⣿⣿⡟⢿⣿⣿⣷⡟⢋⣥⣖⣉⠀⠈⢁⡀⠤⠚⠿⣷⡦⢀⣠⣀⠢⣄⣀⡠⠔⠋⠁⠀⣼⠃⠀⠀',
            '⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⠻⣿⣿⡄⠈⠻⣿⣿⢿⣛⣩⠤⠒⠉⠁⠀⠀⠀⠀⠀⠉⠒⢤⡀⠉⠁⠀⠀⠀⠀⠀⢀⡿⠀⠀⠀',
            '⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⠙⢿⣤⣤⠴⠟⠋⠉⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⠑⠤⠀⠀⠀⠀⠀⢩⠇⠀⠀⠀',
            '⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀',
        ];

        foreach ($lines as $line) {
            $output->writeln('<comment>' . $line . '</comment>');
        }
    }
}
