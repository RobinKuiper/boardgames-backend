<?php

declare(strict_types = 1);

namespace App\Command;

use App\Entity\Role;
use App\Entity\User;
use App\Interfaces\EntityManagerServiceInterface;
use App\Services\HashService;
use DI\NotFoundException;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class AddUser extends Command
{
    protected static $defaultName        = 'app:add:user';
    protected static $defaultDescription = 'Add an user to the database';

    /**
     * @var EntityRepository<Role>
     */
    protected EntityRepository $roleRepository;

    public function __construct(
        protected readonly EntityManagerServiceInterface $entityManager,
        protected readonly HashService $hashService
    ) {
        parent::__construct();

        $objectRepository = $this->entityManager->getRepository(Role::class);
        assert($objectRepository instanceof EntityRepository);
        $this->roleRepository = $objectRepository;
    }

    /**
     * Execute the command.
     *
     * @param InputInterface $input The input interface
     * @param OutputInterface $output The output interface
     *
     * @throws NotFoundException
     *
     * @return int The command return status
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $name  = $this->getUsername($input, $output);
        $email = $this->getEmail($input, $output);
        $role  = $this->getRole($input, $output);

        $password       = $this->hashService->generatePassword();
        $hashedPassword = $this->hashService->hashPassword($password);

        $this->addUser($name, $email, $hashedPassword, $role);

        $output->writeln("New user `$name` has been created");
        $output->writeln("Password: $password");

        return Command::SUCCESS;
    }

    /**
     * Retrieve the username based on user input.
     *
     * @param InputInterface $input The input interface
     * @param OutputInterface $output The output interface
     *
     * @return string The retrieved username
     */
    protected function getUsername(InputInterface $input, OutputInterface $output): string
    {
        /** @var QuestionHelper $helper */
        $helper = $this->getHelper('question');

        $name = '';
        while ($name === '') {
            $question = new Question(
                'Name: ',
                false
            );

            $name = $helper->ask($input, $output, $question);

            if (!is_string($name) || !preg_match("/^[a-zA-Z-' ]*$/", $name)) {
                $output->writeln('Invalid name');
                $name = '';
            }
        }

        return $name;
    }

    /**
     * Retrieve the email based on user input.
     *
     * @param InputInterface $input The input interface
     * @param OutputInterface $output The output interface
     *
     * @return string The retrieved email
     */
    protected function getEmail(InputInterface $input, OutputInterface $output): string
    {
        /** @var QuestionHelper $helper */
        $helper = $this->getHelper('question');

        $email = null;
        while ($email === null) {
            $question = new Question(
                'Email: ',
                false
            );

            $email = $helper->ask($input, $output, $question);

            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $output->writeln('Invalid email format');
                $email = null;
            }
        }

        return $email;
    }

    /**
     * Retrieve the role based on user input.
     *
     * @param InputInterface $input The input interface
     * @param OutputInterface $output The output interface
     *
     * @throws NotFoundException If the role is not found
     *
     * @return Role The retrieved role
     */
    protected function getRole(InputInterface $input, OutputInterface $output): Role
    {
        /** @var QuestionHelper $helper */
        $helper = $this->getHelper('question');
        $roles  = $this->roleRepository->findAll();

        $roleId = null;
        while ($roleId === null) {
            $question = new Question(
                'Role (id): ',
                false
            );

            $roleIds = [];
            /** @var Role $role */
            foreach ($roles as $role) {
                $id   = $role->getId();
                $name = $role->getName();
                $output->writeln("$id: $name");
                $roleIds[] = $id;
            }

            $roleId = $helper->ask($input, $output, $question);

            if (!filter_var($roleId, FILTER_VALIDATE_INT) || !in_array($roleId, $roleIds, false)) {
                $output->writeln("Invalid role `$roleId`");
                $roleId = null;
            }
        }

        if (!$role = $this->entityManager->find(Role::class, $roleId)) {
            throw new NotFoundException('Role not found');
        }

        return $role;
    }

    /**
     * Add a new user.
     *
     * @param string $name The name of the user
     * @param string $email The email of the user
     * @param string $password The password of the user
     * @param Role $role The role of the user
     *
     * @return void
     */
    protected function addUser(string $name, string $email, string $password, Role $role): User
    {
        $user = new User();
        $user->setName($name);
        $user->setEmail($email);
        $user->setPassword($password);
        $user->setRole($role);
        $this->entityManager->sync($user);

        return $user;
    }
}
