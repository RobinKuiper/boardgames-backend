<?php

namespace Tests\Traits;

use App\Interfaces\EntityManagerServiceInterface;
use Clockwork\Clockwork;
use DI\Container;
use DI\ContainerBuilder;
use DI\DependencyException;
use DI\NotFoundException;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Slim\App;
use UnexpectedValueException;

trait AppTestTrait
{
    use ContainerTestTrait;
    use HttpTestTrait;
    use HttpJsonTestTrait;

    protected App $app;

    /**
     * Before each test.
     */
    protected function setUp(): void
    {
        try {
            $this->setUpApp();
        } catch (DependencyException $e) {
            throw new UnexpectedValueException('[Dependency] App must be initialized: ' . $e->getMessage());
        } catch (NotFoundException $e) {
            throw new UnexpectedValueException('[Not-Found] App must be initialized: ' . $e->getMessage());
        } catch (Exception $e) {
            throw new UnexpectedValueException('[General] App must be initialized: ' . $e->getMessage());
        }
    }

    /**
     * @throws DependencyException
     * @throws NotFoundException
     * @throws Exception
     *
     * @return void
     */
    protected function setUpApp(): void
    {
        $container = (new ContainerBuilder())
            ->addDefinitions(__DIR__ . '/../../config/container_bindings.php')
            ->build();

        $container->set(EntityManagerServiceInterface::class, fn (Container $c) => $this->createMock(EntityManagerServiceInterface::class));
        $container->set(EntityManagerInterface::class, fn (Container $c) => $this->createMock(EntityManagerInterface::class));
        $container->set(Clockwork::class, fn (Container $c) => true); // So Clockwork doesn't whine because we mock the EntityManager

        $this->app = $container->get(App::class);

        $this->setUpContainer($container);

        /** @phpstan-ignore-next-line */
        if (method_exists($this, 'setUpDatabase')) {
            $this->setUpDatabase(__DIR__ . '/../../resources/schema/schema.sql');
        }
    }
}
