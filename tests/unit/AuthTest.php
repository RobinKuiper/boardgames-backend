<?php

declare(strict_types = 1);

namespace Tests\unit;

use App\Auth;
use App\Config;
use App\Entity\Role;
use App\Entity\User;
use App\Services\UserProviderService;
use Exception;
use Mockery;
use PHPUnit\Framework\TestCase;
use Zfekete\BypassReadonly\BypassReadonly;

BypassReadonly::enable();

final class AuthTest extends TestCase
{
    private $config;
    private $userProvider;
    private Auth $auth;

    /**
     * Set up the test environment.
     *
     * This method is called before each test case is executed.
     * It prepares the necessary dependencies and configuration for testing.
     * It sets the test secret and algorithm for JWT authentication.
     *
     * @return void
     */
    public function setUp(): void
    {
        $this->config = Mockery::mock(Config::class);
        $this->userProvider = Mockery::mock(UserProviderService::class);

        // Set test secret and algorithm
        $this->config->shouldReceive('get')
          ->with('jwt_authentication.secret')
          ->andReturn('test_secret');
        $this->config->shouldReceive('get')
          ->with('jwt_authentication.algorithm')
          ->andReturn('HS256');

        $this->auth = new Auth($this->userProvider, $this->config);
    }

    public function tearDown(): void
    {
        Mockery::close();
    }

    /**
     * testAttemptLoginSuccess method.
     *
     * This method tests the attemptLogin method in the Auth class when the login is successful.
     *
     * @throws Exception
     *
     * @return void
     */
    public function testAttemptLoginSuccess(): void
    {
        $credentials = ['username' => 'test', 'password' => 'test'];
        $passwordHash = password_hash('test', PASSWORD_DEFAULT);
        $role = new Role();
        $role->setName('role');

        $user = new User();
        $user->setName('test');
        $user->setEmail('test@test.com');
        $user->setRole($role);
        //        $user->addPermission((new Permission(1, 'name'))->setName('name'));
        $user->setPassword($passwordHash);

        $this->userProvider->shouldReceive('getByCredentials')
          ->with($credentials)
          ->andReturn($user);

        $result = $this->auth->attemptLogin($credentials);

        $this->assertIsString($result);
    }

    /**
     * Test the attemptLogin method when login fails due to invalid credentials.
     *
     * This method creates a user with valid credentials and attempts to log in with invalid credentials.
     * It mocks the getByCredentials method of the user provider to return the created user.
     * The expected result is false, indicating that the login attempt has failed.
     *
     * @throws Exception
     *
     * @return void
     */
    public function testAttemptLoginFailureDueToInvalidCredentials(): void
    {
        $credentials = ['username' => 'test', 'password' => 'wrong_test'];
        $passwordHash = password_hash('test', PASSWORD_DEFAULT);
        $role = new Role();
        $role->setName('role');

        $user = new User();
        $user->setName('test');
        $user->setEmail('test@test.com');
        $user->setRole($role);
        //        $user->addPermission((new Permission(1, 'name'))->setName('name'));
        $user->setPassword($passwordHash);

        $this->userProvider->shouldReceive('getByCredentials')
          ->with($credentials)
          ->andReturn($user);

        $result = $this->auth->attemptLogin($credentials);

        $this->assertFalse($result);
    }

    /**
     * @test
     *
     * @throws Exception
     *
     * @return void
     */
    public function testAttemptLoginFailureDueToNonExistentUser(): void
    {
        $credentials = ['username' => 'test', 'password' => 'wrong_test'];

        $this->userProvider->shouldReceive('getByCredentials')
          ->with($credentials)
          ->andReturn(null);

        $result = $this->auth->attemptLogin($credentials);

        $this->assertFalse($result);
    }
}
