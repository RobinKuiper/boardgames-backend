<?php

namespace Tests\unit\Services;

use App\DataObjects\PermissionData;
use App\Entity\Permission;
use App\Interfaces\EntityManagerServiceInterface;
use App\Services\PermissionService;
use PHPUnit\Framework\TestCase;
use Tests\Traits\AppTestTrait;

class PermissionServiceTest extends TestCase
{
    use AppTestTrait;

    public function testCreatePermission()
    {
        $permissionData = new PermissionData('permission_name', 'permission_description');

        $entityManager = $this->container->get(EntityManagerServiceInterface::class);
        $permissionService = new PermissionService($entityManager);

        $createPermission = $permissionService->create($permissionData);

        $this->assertInstanceOf(Permission::class, $createPermission);
        $this->assertSame($permissionData->name, $createPermission->getName());
        $this->assertSame($permissionData->description, $createPermission->getDescription());
    }

    public function testUpdatePermission()
    {
        $existingPermission = $this->createPermission();

        $permissionData = new PermissionData('new_permission', 'new_permission_description');

        $entityManager = $this->container->get(EntityManagerServiceInterface::class);
        $permissionService = new PermissionService($entityManager);

        $updatePermission = $permissionService->update($existingPermission, $permissionData);

        $this->assertSame($permissionData->name, $updatePermission->getName());
        $this->assertSame($permissionData->description, $updatePermission->getDescription());
    }

    public function testPatchPermission()
    {
        $existingPermission = $this->createPermission();

        $permissionData = [
            'name' => 'new_permission',
            'description' => 'new_perm_description',
        ];

        $entityManager = $this->container->get(EntityManagerServiceInterface::class);
        $permissionService = new PermissionService($entityManager);

        $updatePermission = $permissionService->patch($existingPermission, $permissionData);

        $this->assertSame($permissionData['name'], $updatePermission->getName());
        $this->assertSame($permissionData['description'], $updatePermission->getDescription());
    }

    private function createPermission(): Permission
    {
        $permission = new Permission();
        $permission->setName('test_permission_name');
        $permission->setDescription('test_permission description');

        return $permission;
    }
}
