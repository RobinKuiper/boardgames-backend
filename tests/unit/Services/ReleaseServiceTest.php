<?php

namespace Tests\unit\Services;

use App\DataObjects\ReleaseData;
use App\Entity\Product;
use App\Entity\Release;
use App\Interfaces\EntityManagerServiceInterface;
use App\Services\ReleaseService;
use PHPUnit\Framework\TestCase;
use Tests\Traits\AppTestTrait;

class ReleaseServiceTest extends TestCase
{
    use AppTestTrait;

    public function testCreateRelease()
    {
        $product = $this->createProduct();

        $releaseData = new ReleaseData('tag', '12345', $product, true);

        $entityManager = $this->container->get(EntityManagerServiceInterface::class);
        $releaseService = new ReleaseService($entityManager);

        $createdRelease = $releaseService->create($releaseData);

        $this->assertInstanceOf(Release::class, $createdRelease);
        $this->assertSame($releaseData->tag, $createdRelease->getTag());
        $this->assertSame($releaseData->serviceDeskNumber, $createdRelease->getServiceDeskNumber());
        $this->assertSame($releaseData->product, $createdRelease->getProduct());
        $this->assertSame($releaseData->isAcceptance, $createdRelease->getIsAcceptance());
    }

    public function testUpdateRelease()
    {
        $existingRelease = $this->createRelease();
        $newProduct = $this->createProduct();
        $newProduct->setName('test_product_new');

        $releaseData = new ReleaseData('newTag', '67890', $newProduct, true);

        $entityManager = $this->container->get(EntityManagerServiceInterface::class);
        $releaseService = new ReleaseService($entityManager);

        $updatedRelease = $releaseService->update($existingRelease, $releaseData);

        $this->assertSame($releaseData->tag, $updatedRelease->getTag());
        $this->assertSame($releaseData->serviceDeskNumber, $updatedRelease->getServiceDeskNumber());
        $this->assertSame($releaseData->product, $updatedRelease->getProduct());
        $this->assertSame($releaseData->isAcceptance, $updatedRelease->getIsAcceptance());
    }

    private function createProduct(): Product
    {
        $product = new Product();
        $product->setName('test_product');
        $product->setHieraFolderName('test_folder');
        $product->setHasGitlabRelease(true);
        $product->setGitlabProjectId(12345);

        return $product;
    }

    private function createRelease(): Release
    {
        $release = new Release();
        $release->setTag('test_tag');
        $release->setProduct($this->createProduct());
        $release->setIsAcceptance(true);
        $release->setServiceDeskNumber(123);

        return $release;
    }
}
