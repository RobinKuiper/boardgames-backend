<?php

namespace Tests\unit\Services;

use App\DataObjects\RoleData;
use App\Entity\Permission;
use App\Entity\Role;
use App\Interfaces\EntityManagerServiceInterface;
use App\Services\RoleService;
use PHPUnit\Framework\TestCase;
use Tests\Traits\AppTestTrait;

class RoleServiceTest extends TestCase
{
    use AppTestTrait;

    public function testCreateRole()
    {
        $roleData = new RoleData('role_name');

        $entityManager = $this->container->get(EntityManagerServiceInterface::class);
        $roleService = new RoleService($entityManager);

        $createRole = $roleService->create($roleData);

        $this->assertInstanceOf(Role::class, $createRole);
        $this->assertSame($roleData->name, $createRole->getName());
    }

    public function testUpdateRole()
    {
        $existingRole = $this->createRole();

        $roleData = new RoleData('new_role');

        $entityManager = $this->container->get(EntityManagerServiceInterface::class);
        $roleService = new RoleService($entityManager);

        $updateRole = $roleService->update($existingRole, $roleData);

        $this->assertSame($roleData->name, $updateRole->getName());
    }

    public function testPatchRole()
    {
        $existingRole = $this->createRole();

        $roleData = [
            'name' => 'new_role',
        ];

        $entityManager = $this->container->get(EntityManagerServiceInterface::class);
        $roleService = new RoleService($entityManager);

        $updateRole = $roleService->patch($existingRole, $roleData);

        $this->assertSame($roleData['name'], $updateRole->getName());
    }

    public function testAddPermission()
    {
        $existingRole = $this->createRole();
        $existingPermission = $this->createPermission();

        $entityManager = $this->container->get(EntityManagerServiceInterface::class);
        $roleService = new RoleService($entityManager);

        $updateRole = $roleService->addPermission($existingRole, $existingPermission);

        $this->assertContains($existingPermission, $updateRole->getPermissions());
    }

    public function testRemovePermission()
    {
        $existingRole = $this->createRole();
        $existingPermission = $this->createPermission();

        $entityManager = $this->container->get(EntityManagerServiceInterface::class);
        $roleService = new RoleService($entityManager);

        $updateRole = $roleService->addPermission($existingRole, $existingPermission);

        $this->assertContains($existingPermission, $updateRole->getPermissions());

        $updateRole = $roleService->removePermission($existingRole, $existingPermission);

        $this->assertNotContains($existingPermission, $updateRole->getPermissions());
    }

    private function createRole(): Role
    {
        $role = new Role();
        $role->setName('test_role');

        return $role;
    }

    private function createPermission(): Permission
    {
        $permission = new Permission();
        $permission->setName('test_permission');
        $permission->setDescription('test_permission_description');

        return $permission;
    }
}
