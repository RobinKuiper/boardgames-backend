<?php

namespace Tests\unit\Services;

use App\Entity\DeploymentGroup;
use App\Interfaces\EntityManagerServiceInterface;
use App\Services\DeploymentGroupService;
use PHPUnit\Framework\TestCase;
use Tests\Traits\AppTestTrait;

class DeploymentGroupServiceTest extends TestCase
{
    use AppTestTrait;

    public function testCreateDeploymentGroup()
    {
        $name = 'new_deploymentGroup';

        $entityManager = $this->container->get(EntityManagerServiceInterface::class);
        $deploymentGroupService = new DeploymentGroupService($entityManager);

        $createDeploymentGroup = $deploymentGroupService->create($name);

        $this->assertInstanceOf(DeploymentGroup::class, $createDeploymentGroup);
        $this->assertSame($name, $createDeploymentGroup->getName());
    }

    public function testUpdateDeploymentGroup()
    {
        $existingDeploymentGroup = $this->createDeploymentGroup();

        $newName = 'new_deploymentGroup';

        $entityManager = $this->container->get(EntityManagerServiceInterface::class);
        $deploymentGroupService = new DeploymentGroupService($entityManager);

        $updateDeploymentGroup = $deploymentGroupService->update($existingDeploymentGroup, $newName);

        $this->assertSame($newName, $updateDeploymentGroup->getName());
    }

    private function createDeploymentGroup(): DeploymentGroup
    {
        $deploymentGroup = new DeploymentGroup();
        $deploymentGroup->setName('test_deploymentGroup');

        return $deploymentGroup;
    }
}
