<?php

namespace Tests\unit\Services;

use App\DataObjects\EnvironmentData;
use App\Entity\Environment;
use App\Enum\EnvironmentType;
use App\Interfaces\EntityManagerServiceInterface;
use App\Services\EnvironmentService;
use PHPUnit\Framework\TestCase;
use Tests\Traits\AppTestTrait;

class EnvironmentServiceTest extends TestCase
{
    use AppTestTrait;

    public function testCreateEnvironment()
    {
        $environmentData = new EnvironmentData('environment_name', 'environment_host', EnvironmentType::WEB, true);

        $entityManager = $this->container->get(EntityManagerServiceInterface::class);
        $environmentService = new EnvironmentService($entityManager);

        $createEnvironment = $environmentService->create($environmentData);

        $this->assertInstanceOf(Environment::class, $createEnvironment);
        $this->assertSame($environmentData->name, $createEnvironment->getName());
        $this->assertSame($environmentData->host, $createEnvironment->getHost());
        $this->assertSame($environmentData->type, $createEnvironment->getType());
        $this->assertSame($environmentData->isAcceptance, $createEnvironment->getIsAcceptance());
    }

    public function testUpdateEnvironment()
    {
        $existingEnvironment = $this->createEnvironment();

        $environmentData = new EnvironmentData('new_environment_name', 'new_environment_host', EnvironmentType::DATABASE, false);

        $entityManager = $this->container->get(EntityManagerServiceInterface::class);
        $environmentService = new EnvironmentService($entityManager);

        $updateEnvironment = $environmentService->update($existingEnvironment, $environmentData);

        $this->assertSame($environmentData->name, $updateEnvironment->getName());
        $this->assertSame($environmentData->host, $updateEnvironment->getHost());
        $this->assertSame($environmentData->type, $updateEnvironment->getType());
        $this->assertSame($environmentData->isAcceptance, $updateEnvironment->getIsAcceptance());
    }

    public function testPatchEnvironment()
    {
        $existingEnvironment = $this->createEnvironment();

        $environmentData = [
            'name' => 'new_environment_name',
            'host' => 'new_enviroment_host',
            'type' => EnvironmentType::DATABASE,
            'isAcceptance' => false,
        ];

        $entityManager = $this->container->get(EntityManagerServiceInterface::class);
        $environmentService = new EnvironmentService($entityManager);

        $updateEnvironment = $environmentService->patch($existingEnvironment, $environmentData);

        $this->assertSame($environmentData['name'], $updateEnvironment->getName());
        $this->assertSame($environmentData['host'], $updateEnvironment->getHost());
        $this->assertSame($environmentData['type'], $updateEnvironment->getType());
        $this->assertSame($environmentData['isAcceptance'], $updateEnvironment->getIsAcceptance());
    }

    private function createEnvironment(): Environment
    {
        $environment = new Environment();
        $environment->setName('test_environment_name');
        $environment->setHost('test_environment_host');
        $environment->setType(EnvironmentType::WEB);
        $environment->setIsAcceptance(true);

        return $environment;
    }
}
