<?php

declare(strict_types = 1);

namespace Tests\unit\Services;

use App\Services\HashService;
use PHPUnit\Framework\TestCase;

class HashServiceTest extends TestCase
{
    private HashService $hashService;

    protected function setUp(): void
    {
        $this->hashService = new HashService();
    }

    /**
     * Test the hashPassword method.
     */
    public function testHashPassword(): void
    {
        $password = 'testPassword';
        $hashedPassword = $this->hashService->hashPassword($password);
        $this->assertTrue(password_verify($password, $hashedPassword));
    }

    /**
     * Test the generatePassword method.
     */
    public function testGeneratePassword(): void
    {
        $length = 10;
        $password = $this->hashService->generatePassword($length);
        $this->assertEquals($length, strlen($password));
    }
}
