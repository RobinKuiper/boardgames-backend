<?php

namespace Tests\unit\Services;

use App\DataObjects\ClientData;
use App\Entity\Client;
use App\Interfaces\EntityManagerServiceInterface;
use App\Services\ClientService;
use PHPUnit\Framework\TestCase;
use Tests\Traits\AppTestTrait;

class ClientServiceTest extends TestCase
{
    use AppTestTrait;

    public function testCreateClient()
    {
        $clientData = new ClientData('client_name');

        $entityManager = $this->container->get(EntityManagerServiceInterface::class);
        $clientService = new ClientService($entityManager);

        $createClient = $clientService->create($clientData);

        $this->assertInstanceOf(Client::class, $createClient);
        $this->assertSame($clientData->name, $createClient->getName());
    }

    public function testUpdateClient()
    {
        $existingClient = $this->createClient();

        $clientData = new ClientData('new_client');

        $entityManager = $this->container->get(EntityManagerServiceInterface::class);
        $clientService = new ClientService($entityManager);

        $updateClient = $clientService->update($existingClient, $clientData);

        $this->assertSame($clientData->name, $updateClient->getName());
    }

    public function testPatchClient()
    {
        $existingClient = $this->createClient();

        $clientData = [
            'name' => 'new_client',
        ];

        $entityManager = $this->container->get(EntityManagerServiceInterface::class);
        $clientService = new ClientService($entityManager);

        $updateClient = $clientService->patch($existingClient, $clientData);

        $this->assertSame($clientData['name'], $updateClient->getName());
    }

    private function createClient(): Client
    {
        $client = new Client();
        $client->setName('test_client');

        return $client;
    }
}
