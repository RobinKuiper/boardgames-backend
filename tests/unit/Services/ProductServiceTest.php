<?php

namespace Tests\unit\Services;

use App\DataObjects\ProductData;
use App\Entity\Product;
use App\Interfaces\EntityManagerServiceInterface;
use App\Services\ProductService;
use PHPUnit\Framework\TestCase;
use Tests\Traits\AppTestTrait;

class ProductServiceTest extends TestCase
{
    use AppTestTrait;

    public function testCreateProduct()
    {
        $productData = new ProductData('product_name', 'hiera_folder', 13423, true);

        $entityManager = $this->container->get(EntityManagerServiceInterface::class);
        $productService = new ProductService($entityManager);

        $createProduct = $productService->create($productData);

        $this->assertInstanceOf(Product::class, $createProduct);
        $this->assertSame($productData->name, $createProduct->getName());
        $this->assertSame($productData->hieraFolderName, $createProduct->getHieraFolderName());
        $this->assertSame($productData->gitlabProjectId, $createProduct->getGitlabProjectId());
        $this->assertSame($productData->hasGitlabRelease, $createProduct->getHasGitlabRelease());
    }

    public function testUpdateProduct()
    {
        $existingProduct = $this->createProduct();

        $productData = new ProductData('new_product', 'new_hiera_folder_name', 423124, false);

        $entityManager = $this->container->get(EntityManagerServiceInterface::class);
        $productService = new ProductService($entityManager);

        $updateProduct = $productService->update($existingProduct, $productData);

        $this->assertSame($productData->name, $updateProduct->getName());
        $this->assertSame($productData->hieraFolderName, $updateProduct->getHieraFolderName());
        $this->assertSame($productData->gitlabProjectId, $updateProduct->getGitlabProjectId());
        $this->assertSame($productData->hasGitlabRelease, $updateProduct->getHasGitlabRelease());
    }

    public function testPatchProduct()
    {
        $existingProduct = $this->createProduct();

        $productData = [
            'name' => 'new_product',
            'hieraFolderName' => 'new_hiera_folder_name',
            'gitlabProjectId' => 123,
            'hasGitlabRelease' => false,
        ];

        $entityManager = $this->container->get(EntityManagerServiceInterface::class);
        $productService = new ProductService($entityManager);

        $updateProduct = $productService->patch($existingProduct, $productData);

        $this->assertSame($productData['name'], $updateProduct->getName());
        $this->assertSame($productData['hieraFolderName'], $updateProduct->getHieraFolderName());
        $this->assertSame($productData['gitlabProjectId'], $updateProduct->getGitlabProjectId());
        $this->assertSame($productData['hasGitlabRelease'], $updateProduct->getHasGitlabRelease());
    }

    private function createProduct(): Product
    {
        $product = new Product();
        $product->setName('test_product');
        $product->setHieraFolderName('test_folder');
        $product->setHasGitlabRelease(true);
        $product->setGitlabProjectId(12345);

        return $product;
    }
}
