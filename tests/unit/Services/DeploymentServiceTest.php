<?php

namespace Tests\unit\Services;

use App\Config;
use App\DataObjects\DeploymentData;
use App\Entity\Client;
use App\Entity\Deployment;
use App\Entity\DeploymentGroup;
use App\Entity\Environment;
use App\Entity\Product;
use App\Entity\Release;
use App\Enum\EnvironmentType;
use App\Interfaces\EntityManagerServiceInterface;
use App\Services\DeploymentService;
use PHPUnit\Framework\TestCase;
use Tests\Traits\AppTestTrait;

class DeploymentServiceTest extends TestCase
{
    use AppTestTrait;

    public function testCreateDeployment()
    {
        $deploymentGroup = $this->createDeploymentGroup();
        $client = $this->createClient();
        $webEnvironment = $this->createEnvironment(EnvironmentType::WEB);
        $dbEnvironment = $this->createEnvironment(EnvironmentType::DATABASE);
        $deploymentData = new DeploymentData('https://google.nl', $deploymentGroup, $client, $webEnvironment, $dbEnvironment);

        $entityManager = $this->container->get(EntityManagerServiceInterface::class);
        $config = $this->container->get(Config::class);
        $deploymentService = new DeploymentService($entityManager, $config);

        $createDeployment = $deploymentService->create($deploymentData);

        $this->assertInstanceOf(Deployment::class, $createDeployment);
        $this->assertSame($deploymentData->url, $createDeployment->getUrl());
        $this->assertSame($deploymentData->deploymentGroup, $createDeployment->getDeploymentGroup());
        $this->assertSame($deploymentData->client, $createDeployment->getClient());
        $this->assertSame($deploymentData->webEnvironment, $createDeployment->getWebEnvironment());
        $this->assertSame($deploymentData->dbEnvironment, $createDeployment->getDbEnvironment());
    }

    public function testUpdateDeployment()
    {
        $existingDeployment = $this->createDeployment();

        $deploymentGroup = $this->createDeploymentGroup();
        $client = $this->createClient();
        $webEnvironment = $this->createEnvironment(EnvironmentType::WEB);
        $dbEnvironment = $this->createEnvironment(EnvironmentType::DATABASE);
        $deploymentData = new DeploymentData('https://google.nl', $deploymentGroup, $client, $webEnvironment, $dbEnvironment);

        $entityManager = $this->container->get(EntityManagerServiceInterface::class);
        $config = $this->container->get(Config::class);
        $deploymentService = new DeploymentService($entityManager, $config);

        $updateDeployment = $deploymentService->update($existingDeployment, $deploymentData);

        $this->assertSame($deploymentData->url, $updateDeployment->getUrl());
        $this->assertSame($deploymentData->deploymentGroup, $updateDeployment->getDeploymentGroup());
        $this->assertSame($deploymentData->client, $updateDeployment->getClient());
        $this->assertSame($deploymentData->webEnvironment, $updateDeployment->getWebEnvironment());
        $this->assertSame($deploymentData->dbEnvironment, $updateDeployment->getDbEnvironment());
    }

    public function testUpgradeRelease()
    {
        $existingDeployment = $this->createDeployment();
        $existingRelease = $this->createRelease();

        $entityManager = $this->container->get(EntityManagerServiceInterface::class);
        $config = $this->container->get(Config::class);
        $deploymentService = new DeploymentService($entityManager, $config);

        $updateDeployment = $deploymentService->upgradeRelease($existingDeployment, $existingRelease);

        $this->assertSame($existingRelease, $updateDeployment->getRelease());
    }

    private function createDeployment(): Deployment
    {
        $deployment = new Deployment();
        $deployment->setUrl('https://updated.com');
        $deployment->setClient($this->createClient());
        $deployment->setRelease($this->createRelease());
        $deployment->setDeploymentGroup($this->createDeploymentGroup());
        $deployment->setWebEnvironment($this->createEnvironment(EnvironmentType::WEB));
        $deployment->setDbEnvironment($this->createEnvironment(EnvironmentType::DATABASE));

        return $deployment;
    }

    private function createClient(): Client
    {
        $client = new Client();
        $client->setName('test_client');

        return $client;
    }

    private function createDeploymentGroup(): DeploymentGroup
    {
        $deploymentGroup = new DeploymentGroup();
        $deploymentGroup->setName('test_deployment_group');
        $deploymentGroup->setIsAcceptance(false);

        return $deploymentGroup;
    }

    private function createEnvironment(EnvironmentType $type): Environment
    {
        $environment = new Environment();
        $environment->setName('test_environment');
        $environment->setHost('test_host');
        $environment->setIsAcceptance(true);
        $environment->setType($type);

        return $environment;
    }

    private function createProduct(): Product
    {
        $product = new Product();
        $product->setName('test_product');
        $product->setHieraFolderName('test_folder');
        $product->setHasGitlabRelease(true);
        $product->setGitlabProjectId(12345);

        return $product;
    }

    private function createRelease(): Release
    {
        $release = new Release();
        $release->setTag('test_tag');
        $release->setProduct($this->createProduct());
        $release->setIsAcceptance(true);
        $release->setServiceDeskNumber(123);
        $release->setCreatedAt(new \DateTime('now'));

        return $release;
    }
}
