<?php

namespace Tests\unit\Enum;

use App\Enum\EnvironmentType;
use PHPUnit\Framework\TestCase;

class EnvironmentTypeTest extends TestCase
{
    /**
     * @test
     */
    public function enumValues()
    {
        $this->assertEquals(
            [EnvironmentType::WEB, EnvironmentType::DATABASE],
            EnvironmentType::cases()
        );
    }

    /**
     * @test
     */
    public function invalidEnumValue()
    {
        $value = EnvironmentType::tryFrom('INVALID_VALUE');

        $this->assertSame(null, $value);
    }

    /**
     * @test
     */
    public function enumToString()
    {
        $this->assertEquals('webserver', EnvironmentType::WEB->value);
        $this->assertEquals('database server', EnvironmentType::DATABASE->value);
    }
}
