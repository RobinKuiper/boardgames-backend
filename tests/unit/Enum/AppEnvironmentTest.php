<?php

namespace Tests\unit\Enum;

use App\Enum\AppEnvironment;
use PHPUnit\Framework\TestCase;

class AppEnvironmentTest extends TestCase
{
    /**
     * @test
     */
    public function enumValues()
    {
        $this->assertEquals(
            [AppEnvironment::Development, AppEnvironment::Production],
            AppEnvironment::cases()
        );
    }

    /**
     * @test
     */
    public function invalidEnumValue()
    {
        $value = AppEnvironment::tryFrom('INVALID_VALUE');

        $this->assertSame(null, $value);
    }

    /**
     * @test
     */
    public function enumToString()
    {
        $this->assertEquals('development', AppEnvironment::Development->value);
        $this->assertEquals('production', AppEnvironment::Production->value);
    }

    /**
     * @test
     */
    public function isProduction()
    {
        $this->assertTrue(AppEnvironment::isProduction('production'));
        $this->assertFalse(AppEnvironment::isProduction('development'));
    }

    /**
     * @test
     */
    public function isDevelopment()
    {
        $this->assertTrue(AppEnvironment::isDevelopment('development'));
        $this->assertFalse(AppEnvironment::isDevelopment('production'));
    }
}
