<?php

declare(strict_types = 1);

namespace Tests\unit;

use App\ResponseFormatter;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;

class ResponseFormatterTest extends TestCase
{
    private ResponseFormatter $formatter;

    protected function setUp(): void
    {
        $this->formatter = new ResponseFormatter();
    }

    public function testAsJson(): void
    {
        $response = $this->createMock(ResponseInterface::class);
        $body = $this->createMock(StreamInterface::class);

        $data = ['key' => 'value'];
        $dataJson = json_encode($data, JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_QUOT | JSON_HEX_APOS | JSON_THROW_ON_ERROR);

        $response->expects($this->once())
          ->method('withHeader')
          ->with('Content-Type', 'application/json')
          ->willReturnSelf();

        $response->expects($this->once())
          ->method('getBody')
          ->willReturn($body);

        $body->expects($this->once())
          ->method('write')
          ->with($dataJson);

        $newResponse = $this->formatter->asJson($response, $data);

        $this->assertInstanceOf(ResponseInterface::class, $newResponse);
    }
}
