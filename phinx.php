<?php

declare(strict_types = 1);

$container = require __DIR__ . '/bootstrap.php';

$config = [
  'adapter'  => $_ENV['DB_ADAPTER'] ?? 'mysql',
  'host'     => $_ENV['DB_HOST'] ?? 'localhost',
  'port'     => $_ENV['DB_PORT'] ?? DEFAULT_DB_PORT,
  'name'     => $_ENV['DB_DATABASE'],
  'user'     => $_ENV['DB_USER'],
  'pass'     => $_ENV['DB_PASS'],
];

return
[
    'paths'         => [
        'migrations' => CONFIG_PATH.'/db/migrations',
        'seeds'      => CONFIG_PATH.'/db/seeds'
    ],
    'environments'  => [
        'default_environment' => 'development',
        'development'         => $config,
    ],
    'version_order' => 'creation'
];
