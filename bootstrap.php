<?php

declare(strict_types = 1);

use Dotenv\Dotenv;
use Dotenv\Exception\InvalidEncodingException;
use Dotenv\Exception\InvalidFileException;
use Dotenv\Exception\InvalidPathException;

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/config/constants.php';

$dotenv = Dotenv::createImmutable(__DIR__);
try {
    $dotenv->load();
} catch (InvalidEncodingException|InvalidFileException|InvalidPathException $e) {
    echo "\033[31m.env file not found\033[0m" . PHP_EOL;
    echo "Please copy the .env.example in the root to .env and set the values before trying again." . PHP_EOL;
    throw new RuntimeException(".env file not found");
}

return require_once CONFIG_PATH . '/container.php';
