[//]: # ([![Website]&#40;https://img.shields.io/badge/Visit-Website-blue?logo=google-chrome&#41;]&#40;https://www.remnant-tools.com&#41;)
[![Latest Release](https://gitlab.com/RobinKuiper/deploymanager/-/badges/release.svg)](https://gitlab.com/RobinKuiper/deploymanager/-/releases)
[![pipeline status](https://gitlab.com/RobinKuiper/deploymanager/badges/develop/pipeline.svg)](https://gitlab.com/RobinKuiper/deploymanager/-/commits/develop)

# Install

1. Copy the `.env.example` to `.env` in the root of the project.
2. Edit the configuration options in the `.env` (See below)
3. Install the dependencies with `composer install`
4. Run `composer setup`
    * This will set the environment
    * This will run the database migrations.
    * This will generate a `JWT_SECRET` (or ask if already set).
    * This will interactively create an admin user.
    * (development only) This will ask if you want to seed the database.

# Docker development

1. Follow steps `1` and `2` above, match the database settings with the settings in step `4` below.
2. Go to `./docker`
3. Copy the `.env.example` to `.env` in the `docker` directory.
4. Edit the configuration options in the `.env` (See below)
5. Run `docker compose up -d`
6. Do step `4` from `Install` above.

### Options
| **Option**                | **Description**                            | **Options**                | **Default**         | **Extra**                                 |
|---------------------------|--------------------------------------------|----------------------------|---------------------|-------------------------------------------|
| APP_NAME                  | The name of the application                |                            | Deployment Manager  |                                           |
| APP_ENV                   | Environment the app is running in          | development \| production  | development         | Can be set with the install command       |
| APP_URL                   | The url the app will be running in         |                            |                     |                                           |
| APP_DEBUG                 | If you want to enable debug functionality  | yes \| no \| true \| false |                     |                                           |
| PUPPET_HIERA_PATH         | Path to the Puppet hiera                   |                            |                     |                                           | 
| ALLOWED_ORIGIN            | Url of the client                          |                            |                     |                                           |
| MAILER_SMTP_HOST          | Host of the SMTP mailserver                |                            |                     |                                           |
| MAILER_SMTP_PORT          | Port of the SMTP mailserver                |                            |                     |                                           |
| MAILER_USE_AUTHENTICATION | If the SMTP mailserver uses authentication | yes \| no \| true \| false | false               |                                           |
| MAILER_FROM               | Mail address to mail from                  |                            | test@example.com    |                                           |
| MAILER_USERNAME           | Username of the SMTP mailserver            |                            |                     |                                           |
| MAILER_PASSWORD           | Password of the SMTP mailserver            |                            |                     |                                           |
| JWT_SECRET                | A secret token for authentication          |                            |                     | Can be generated with the install command |
| GITLAB_API_URL            | The Gitlab instance url                    |                            | https://gitlab.com/ |                                           |
| GITLAB_TOKEN              | The Gitlab API token for authentication    |                            |                     |                                           |
| DB_HOST                   | Host of the database server                |                            | localhost           |                                           |
| DB_PORT                   | Port of the database server                |                            | 3306                |                                           |
| DB_DRIVER                 | Database driver                            |                            | pdo_mysql           |                                           |
| DB_USER                   | Username for the database connection       |                            |                     |                                           |
| DB_PASS                   | Password for the database connection       |                            |                     |                                           |
| DB_DATABASE               | Database name for the database connection  |                            |                     |                                           |

### Commands

| **Command**                   | **Description**                                          |
|-------------------------------|----------------------------------------------------------|
| `composer cs:check`           | Check formatting                                         |
| `composer cs:fix`             | Fix formatting                                           |
| `composer grum`               | Run git hooks over code base                             |
| `composer jwt:generate`       | Generate new JWT secret key                              |
| `composer migrate`            | Run database migrations                                  |
| `composer migrate:undo`       | Undo database migrations                                 |
| `composer migration:create`   | Create new database migration file                       |
| `composer migration:generate` | Generate new database migration file based on difference |
| `composer seed`               | Seed the database with test data                         |
| `composer stan:baseline`      | Set the baseline for PHPStan                             |
| `composer test`               | Run the unit tests                                       |
| `composer test:coverage`      | Run the unit tests coverage                              |
| `composer test:complete`      | Run the unit tests with everything                       |
| `php bin/app`                 | List app commands                                        |
